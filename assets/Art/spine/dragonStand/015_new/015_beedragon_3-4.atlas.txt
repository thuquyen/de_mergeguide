
015_beedragon_3-4.png
size: 475,518
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 133
  size: 203, 383
  orig: 203, 383
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 295, 65
  size: 82, 80
  orig: 82, 80
  offset: 0, 0
  index: -1
11
  rotate: true
  xy: 386, 107
  size: 58, 87
  orig: 58, 87
  offset: 0, 0
  index: -1
12
  rotate: true
  xy: 397, 275
  size: 41, 54
  orig: 41, 54
  offset: 0, 0
  index: -1
13
  rotate: true
  xy: 295, 5
  size: 58, 69
  orig: 58, 69
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 207, 147
  size: 177, 115
  orig: 177, 115
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 2, 2
  size: 188, 129
  orig: 188, 129
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 379, 47
  size: 67, 58
  orig: 67, 58
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 192, 17
  size: 101, 114
  orig: 101, 114
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 386, 167
  size: 87, 95
  orig: 87, 95
  offset: 0, 0
  index: -1
Hue/Saturation 2
  rotate: false
  xy: 397, 318
  size: 70, 90
  orig: 70, 90
  offset: 0, 0
  index: -1
Hue/Saturation 3
  rotate: false
  xy: 397, 410
  size: 69, 106
  orig: 69, 106
  offset: 0, 0
  index: -1
Layer 1269
  rotate: false
  xy: 207, 264
  size: 188, 252
  orig: 188, 252
  offset: 0, 0
  index: -1
Layer 1270
  rotate: true
  xy: 397, 264
  size: 9, 13
  orig: 9, 13
  offset: 0, 0
  index: -1
