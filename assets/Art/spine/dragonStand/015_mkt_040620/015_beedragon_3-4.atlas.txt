
015_beedragon_3-4.png
size: 128,512
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 299
  size: 78, 147
  orig: 78, 147
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 2, 30
  size: 31, 31
  orig: 31, 31
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 82, 323
  size: 22, 34
  orig: 22, 34
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 76, 276
  size: 16, 21
  orig: 16, 21
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 2, 2
  size: 22, 26
  orig: 22, 26
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 2, 102
  size: 68, 44
  orig: 68, 44
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 2, 148
  size: 72, 50
  orig: 72, 50
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 82, 299
  size: 26, 22
  orig: 26, 22
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 82, 402
  size: 39, 44
  orig: 39, 44
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 2, 63
  size: 33, 37
  orig: 33, 37
  offset: 0, 0
  index: -1
Hue/Saturation 2
  rotate: false
  xy: 37, 65
  size: 27, 35
  orig: 27, 35
  offset: 0, 0
  index: -1
Hue/Saturation 3
  rotate: false
  xy: 82, 359
  size: 27, 41
  orig: 27, 41
  offset: 0, 0
  index: -1
Layer 1269
  rotate: false
  xy: 2, 200
  size: 72, 97
  orig: 72, 97
  offset: 0, 0
  index: -1
Layer 1270
  rotate: false
  xy: 111, 395
  size: 4, 5
  orig: 4, 5
  offset: 0, 0
  index: -1
