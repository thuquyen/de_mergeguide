
z4.png
size: 100,100
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: false
  xy: 39, 60
  size: 55, 16
  orig: 55, 16
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 50
  size: 35, 45
  orig: 35, 45
  offset: 0, 0
  index: -1
bodyup
  rotate: false
  xy: 2, 12
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
tail
  rotate: true
  xy: 39, 36
  size: 22, 36
  orig: 22, 36
  offset: 0, 0
  index: -1
teeth
  rotate: false
  xy: 77, 43
  size: 17, 15
  orig: 17, 15
  offset: 0, 0
  index: -1
wings
  rotate: false
  xy: 39, 78
  size: 58, 17
  orig: 58, 17
  offset: 0, 0
  index: -1
