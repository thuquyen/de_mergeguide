
013_BlueEye_top.png
size: 128,128
format: RGBA8888
filter: Linear,Linear
repeat: none
10
  rotate: false
  xy: 95, 31
  size: 10, 21
  orig: 10, 21
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 111, 62
  size: 15, 24
  orig: 15, 24
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 74, 26
  size: 19, 26
  orig: 19, 26
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 25
  size: 27, 70
  orig: 27, 70
  offset: 0, 0
  index: -1
hand
  rotate: true
  xy: 63, 6
  size: 17, 11
  orig: 17, 11
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 111, 88
  size: 15, 37
  orig: 15, 37
  offset: 0, 0
  index: -1
tail
  rotate: true
  xy: 2, 2
  size: 21, 59
  orig: 21, 59
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 2, 54
  size: 107, 71
  orig: 107, 71
  offset: 0, 0
  index: -1
