
004_Bahamut_top.png
size: 240,240
format: RGBA8888
filter: Linear,Linear
repeat: none
2
  rotate: true
  xy: 1, 1
  size: 20, 29
  orig: 20, 29
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 118, 69
  size: 29, 34
  orig: 29, 34
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 118, 48
  size: 17, 19
  orig: 17, 19
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 1, 23
  size: 80, 115
  orig: 80, 115
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 204, 97
  size: 31, 140
  orig: 31, 140
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 1, 105
  size: 201, 132
  orig: 201, 132
  offset: 0, 0
  index: -1
