
023_Top.png
size: 110,521
format: RGBA8888
filter: Linear,Linear
repeat: none
back
  rotate: false
  xy: 45, 0
  size: 45, 63
  orig: 45, 63
  offset: 0, 0
  index: -1
back_leg
  rotate: false
  xy: 84, 280
  size: 24, 38
  orig: 24, 38
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 0, 2
  size: 43, 134
  orig: 43, 134
  offset: 0, 0
  index: -1
chain
  rotate: false
  xy: 0, 138
  size: 69, 22
  orig: 69, 22
  offset: 0, 0
  index: -1
front_leg
  rotate: false
  xy: 45, 100
  size: 23, 36
  orig: 23, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 45, 65
  size: 48, 33
  orig: 48, 33
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 71, 125
  size: 28, 22
  orig: 28, 22
  offset: 0, 0
  index: -1
shoulder
  rotate: false
  xy: 92, 383
  size: 16, 27
  orig: 16, 27
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 84, 149
  size: 23, 129
  orig: 23, 129
  offset: 0, 0
  index: -1
tail_circle
  rotate: false
  xy: 0, 235
  size: 82, 83
  orig: 82, 83
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 0, 162
  size: 70, 71
  orig: 70, 71
  offset: 0, 0
  index: -1
wing_circle1
  rotate: false
  xy: 0, 320
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
wing_circle2
  rotate: false
  xy: 0, 412
  size: 110, 109
  orig: 110, 109
  offset: 0, 0
  index: -1
