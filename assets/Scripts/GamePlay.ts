import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlay extends cc.Component {

    @property(cc.Node)
    iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    newDragon: cc.Node = null;

    @property(path)
    path1: path = null;

    @property(path)
    path2: path = null;

    @property(cc.Node)
    btnEgg: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Node)
    cloud: cc.Node = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    @property(cc.Node)
    notification: cc.Node = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    @property(cc.Node)
    button: cc.Node = null;

    @property(cc.Node)
    handTouch: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node = null;

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 10;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;

    onLoad() {
        this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
                this.scheduleOnce(() => {
                    if (this.ironsource)
                        window.NUC.trigger.autoplay();
                    this.showEndGame();
                }, 18);
            }
        }, this);
        this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');
    }

    start() {
        this.button.zIndex = 6;
        this.endGame.zIndex = 10;
        this.notification.zIndex = 4;
        this.cloud.zIndex = 1;
        this.btnEgg.zIndex = 2;
        this.btnEgg.on('click', this.spawnDragon, this);

        this.path1.onComlete = () => {
            this.path1.destroy();
            this.path2.node.active = true;
        }

        this.path2.onComlete = () => {
            this.path2.destroy();
            this.showEndGame();
        }
    }

    spawnDragon() {
        if (this.stateStart) {
            this.stateStart = false;
            this.scheduleOnce(() => {
                if (this.ironsource)
                    window.NUC.trigger.autoplay();
                this.showEndGame();
            }, 18);
        }
        this.tutorialMerge.zIndex = 5;
        this.tutorialMerge.active = true;
        Global.start = true;
        if (this.handTouch.isValid)
            this.handTouch.destroy();
            
        if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            if (Global.numberDragon <= 0) {
                for (let i = 0; i < this.notification.childrenCount; i++) {
                    if (this.notification.children[i].name === "txt_eggIsNotReady" || this.notification.children[i].name === "frame_shop_2") {
                        this.notification.children[i].active = true;
                    } else {
                        this.notification.children[i].active = false;
                    }
                }
                this.notification.getComponent(cc.Animation).play("animNoti");

                if (this.stateNoti) {
                    this.stateNoti = false;
                    this.scheduleOnce(() => {
                        this.notification.getComponent(cc.Animation).play("animNotiOff");
                        this.stateNoti = true;
                    }, 2);
                }


            } else {
                cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                Global.numberDragonInScreen++;
                Global.numberDragon--;
                if (Global.numberDragon <= 0) {
                    Global.numberDragon = 0;
                }
                Global.clickEgg = true;
                let dragon = cc.instantiate(this.dragons[0]);
                this.spawnfxEgg();
                dragon.parent = cc.Canvas.instance.node;
                dragon.x = this.btnEgg.x;
                dragon.y = this.btnEgg.y;
                dragon.scale = 0.2;
                dragon.getComponent("moveDragon").enabled = false;
                this.spawnfxFlyDragon(dragon);
                dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() - 0.5) * 0.8), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                    cc.scaleTo(1, 1).easing(cc.easeSineOut())),
                    cc.callFunc(() => {
                        dragon.getComponent("moveDragon").enabled = true;
                    })));
            }
        } else {
            for (let i = 0; i < this.notification.childrenCount; i++) {
                if (this.notification.children[i].name === "txt_maxDragon" || this.notification.children[i].name === "frame_shop_2") {
                    this.notification.children[i].active = true;
                } else {
                    this.notification.children[i].active = false;
                }
            }
            this.notification.getComponent(cc.Animation).play("animNoti");

            if (this.stateNoti) {
                this.stateNoti = false;
                this.scheduleOnce(() => {
                    this.notification.getComponent(cc.Animation).play("animNotiOff");
                    this.stateNoti = true;
                }, 2);
            }
        }
    }

    spawnfxEgg() {
        let fxEgg = cc.instantiate(this.fxClickEgg);
        fxEgg.parent = cc.Canvas.instance.node;
        fxEgg.x = this.btnEgg.x - 5;
        fxEgg.y = this.btnEgg.y + 10;
        fxEgg.zIndex = 4;
    }

    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        if (this.stateShowEndGame) {
            if (this.ironsource)
                window.NUC.trigger.endGame('fake');
            if (cc.Canvas.instance.node.getChildByName("bgLayout")) {
                if (cc.Canvas.instance.node.getChildByName("bgLayout").isValid)
                    cc.Canvas.instance.node.getChildByName("bgLayout").destroy();
            }

            if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
            }
            this.scheduleOnce(() => {
                this.newDragon.runAction(cc.moveTo(2, cc.v2(0, -330)));
            }, 1);
            this.stateShowEndGame = false;
            Global.endGame = true;
            this.endGame.active = true;
            cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            this.btnEgg.off('click');
            if (this.button)
                this.button.active = false;
        }
    }

}
