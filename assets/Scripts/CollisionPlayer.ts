import Global from "./Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    count: number = 0;
    count1: number=0;
    start() {

    }
    onCollisionEnter(other, self) {
        if (other.node.name === 'BlueEye_top_non1') {
            this.count++;
            if (this.count == 2) {
                this.scheduleOnce(() => {
                    other.node.active = false;
                    self.node.active = false;
                    Global.isTransform = true;
                }, 0.2)
            }
        }
        else
        if (other.node.name === 'Firedragon_top_non1') {
            this.count1++;
            if (this.count1 == 2) {
                this.scheduleOnce(() => {
                    other.node.active = false;
                    self.node.active = false;
                    Global.isTransform2 = true;
                }, 0.2)
            }
        }
    }
    // update (dt) {}
}
