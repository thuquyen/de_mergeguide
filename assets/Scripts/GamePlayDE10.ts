import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE5 extends cc.Component {

    @property(cc.Node)
    iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    btnEgg: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    @property(cc.Node)
    btnIns: cc.Node = null;

    @property(cc.Node)
    handTouch: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node[] = [];

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 2;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;
    adcolony: boolean = false;

    @property(cc.Node)
    listDragon: cc.Node[] = [];
    run1st: boolean = true;
    run2nd: boolean = true;
    run3rd: boolean = true;

    totalWave: number = 0;
    checkTotalW: boolean = false;
    checkWin1: boolean = false;
    checkeWin2: boolean = false;

    @property(cc.Node)
    menu: cc.Node = null;
    click1: boolean = false;
    click2: boolean = false;
    click3: boolean = false;
    click4: boolean = false;
    checkauto: boolean = false;
    isClickEgg: boolean = false;

    @property(cc.Node)
    waveNext: cc.Node = null;
    nextWave: boolean = null;
    isEnd: boolean = false;
    @property(cc.Node)
    bgEnd: cc.Node = null;
    @property(cc.Node)
    handEnd: cc.Node
    @property(cc.Node)
    endDragon: cc.Node = null;
    onLoad() {
        // this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;

        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
            }
        }, this);
        // this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');

        if (this.adcolony) {
            window.mraid.isViewable;
        }
    }

    start() {
        this.scheduleOnce(() => {
            this.clickDragon2();
            if (this.ironsource)
                window.NUC.trigger.autoplay();
        }, 10);
        // this.button.zIndex = 6;
        this.endGame.zIndex = 10;
        this.btnEgg.zIndex = 2;
        //click to open egg
        this.btnEgg.on('click', this.spawnDragonClick, this);
    }
    showHandClick() {
        this.handTouch.active = true;
        this.handTouch.zIndex = 10;
        this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');
    }

    update(dt) {
        if (Global.totalE == 0 && !this.checkTotalW) {
            this.checkTotalW = true;
            Global.endGame = true;
            if (this.ironsource && !this.checkWin1) {
                this.checkWin1 = true;
                window.NUC.trigger.endGame('win');
            }
            this.scheduleOnce(() => {
                this.showEndGame();
            }, 2)
        }
        if (Global.totalEIdle >= 6 && !this.nextWave) {
            this.nextWave = true;
            this.waveNext.getComponent("WaveEChangePos").enabled = true;
        }

        if (Global.mergeNumber == 1 && this.run1st) {
            this.isClickEgg = true;
            this.showHandClick();
            for (var i = 0; i < 2; i++) {
                this.tutorialMerge[i].destroy();
            }
            this.tutorialMerge[2].zIndex = 5;
            this.tutorialMerge[2].active = true;
            this.run1st = false;
        }
        else if (Global.mergeNumber == 2 && this.run2nd) {
            this.handTouch.opacity = 255;
            this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
            this.run2nd = false;
            this.spawnDragon(this.dragons[0]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[2]);
            }, 0.5)
        }
        else if (Global.mergeNumber == 3 && this.run3rd) {
            this.run3rd = false;
            this.spawnDragon(this.dragons[0]);
            this.spawnDragon(this.dragons[2]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[1]);
            }, 0.5)
        }
        else if (Global.mergeNumber >= 7) {
            if (this.ironsource && !this.checkeWin2) {
                this.checkeWin2 = true;
                window.NUC.trigger.endGame('win')
            }
            if (!this.isEnd) {
                Global.endGame = true;
                this.isEnd = true;
                this.scheduleOnce(() => {
                    this.showEndGame();
                }, 2)
            }
        }
    }

    spawnDragon(dragonNode: cc.Prefab) {
        if (this.stateStart) {
            this.stateStart = false;
        }
        Global.start = true;
        // when clicked guide text will off
        if (this.handTouch.isValid) {
            this.handTouch.opacity = 0;
            this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
        }
        if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            cc.Camera.main.getComponent(cc.Animation).play("animCamera");
            Global.numberDragonInScreen++;
            Global.numberDragon--;
            if (Global.numberDragon <= 0) {
                Global.numberDragon = 0;
            }
            Global.clickEgg = true;
            let dragon = cc.instantiate(dragonNode);
            this.spawnfxEgg();
            dragon.parent = cc.Canvas.instance.node;
            dragon.x = this.btnEgg.x;
            dragon.y = this.btnEgg.y;
            dragon.scale = 0.2;
            dragon.getComponent("MoveDragonDE4").enabled = false;
            this.spawnfxFlyDragon(dragon);
            dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.5), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                cc.callFunc(() => {
                    dragon.getComponent("MoveDragonDE4").enabled = true;
                })));
        }
    }

    spawnDragonClick() {
        if (this.isClickEgg) {
            this.tutorialMerge[2].active = false;
            this.isClickEgg = false;
            // when clicked guide text will off
            if (this.handTouch.isValid) {
                this.handTouch.opacity = 0;
                this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
            }

            if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
                cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                Global.numberDragonInScreen++;
                Global.numberDragon--;
                if (Global.numberDragon <= 0) {
                    Global.numberDragon = 0;
                }
                let dragon = cc.instantiate(this.dragons[0]);
                this.spawnfxEgg();
                dragon.parent = cc.Canvas.instance.node;
                dragon.x = this.btnEgg.x;
                dragon.y = this.btnEgg.y;
                dragon.scale = 0.2;
                dragon.getComponent("MoveDragonDE4").enabled = false;
                this.spawnfxFlyDragon(dragon);
                dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.5), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                    cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                    cc.callFunc(() => {
                        dragon.getComponent("MoveDragonDE4").enabled = true;
                    })));

                let dragon1 = cc.instantiate(this.dragons[1]);
                this.spawnfxEgg();
                dragon1.parent = cc.Canvas.instance.node;
                dragon1.x = this.btnEgg.x;
                dragon1.y = this.btnEgg.y;
                dragon1.scale = 0.2;
                dragon1.getComponent("MoveDragonDE4").enabled = false;
                this.spawnfxFlyDragon(dragon1);
                dragon1.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.5), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                    cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                    cc.callFunc(() => {
                        dragon1.getComponent("MoveDragonDE4").enabled = true;
                    })));
            }
        }
    }

    spawnfxEgg() {
        let fxEgg = cc.instantiate(this.fxClickEgg);
        fxEgg.parent = cc.Canvas.instance.node;
        fxEgg.x = this.btnEgg.x - 5;
        fxEgg.y = this.btnEgg.y + 10;
        fxEgg.zIndex = 4;
    }

    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        this.bgEnd.active = true;
        this.clickDragonEnd();
        if (this.stateShowEndGame) {
            this.btnIns.active = false;
            this.stateShowEndGame = false;
            this.endGame.active = true;
            // cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            this.btnEgg.destroy();

            var findDeep = function (node, nodeName) {
                if (node.name == nodeName) return node;
                for (var i = 0; i < node.children.length; i++) {
                    var res = findDeep(node.children[i], nodeName);
                    if (res)
                        res.destroy();
                    // return res;
                }
            }
            findDeep(cc.director.getScene(), "z2E");
            findDeep(cc.director.getScene(), "e_I1");
            findDeep(cc.director.getScene(), "e_H2");
            findDeep(cc.director.getScene(), "BulletZigZig");
        }
    }

    clickIns1() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnStart')
        }
    }
    clickIns2() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnEnd')
        }
    }

    clickDragon1() {
        if (this.menu.opacity == 255 && !this.click1) {
            if (this.ironsource) {
                window.NUC.event.send('chooseDragon', 'Bubble')
            }
            this.click1 = true;
            this.menu.opacity = 0;
            this.listDragon[0].runAction(cc.sequence(
                cc.moveBy(1, cc.v2(0, 410)),
                cc.callFunc(() => {
                    this.listDragon[0].getComponent("MoveDragonDE4").enabled = true;
                })
            ))
            this.scheduleOnce(() => {
                this.listDragon[1].runAction(cc.sequence(
                    cc.moveBy(1, cc.v2(0, 410)),
                    cc.callFunc(() => {
                        this.listDragon[1].getComponent("MoveDragonDE4").enabled = true;
                    })
                ))
            })
            this.scheduleOnce(() => {
                this.tutorialMerge[0].zIndex = 5;
                this.tutorialMerge[0].active = true;
            }, 1)
        }
    }
    clickDragon2() {
        if (this.menu.opacity == 255 && !this.click2) {
            if (this.ironsource && !this.checkauto) {
                window.NUC.event.send('chooseDragon', 'BlueEye')
            }

            this.click2 = true;
            this.menu.opacity = 0;
            this.listDragon[2].runAction(cc.sequence(
                cc.moveBy(1, cc.v2(0, 410)),
                cc.callFunc(() => {
                    this.listDragon[2].getComponent("MoveDragonDE4").enabled = true;
                })
            ))
            this.scheduleOnce(() => {
                this.listDragon[3].runAction(cc.sequence(
                    cc.moveBy(1, cc.v2(0, 410)),
                    cc.callFunc(() => {
                        this.listDragon[3].getComponent("MoveDragonDE4").enabled = true;
                    })
                ))
            })
            this.scheduleOnce(() => {
                this.tutorialMerge[1].zIndex = 5;
                this.tutorialMerge[1].active = true;
            }, 1)
        }
    }

    // clickDragon3() {
    //     if (this.menu.opacity == 255 && !this.click3) {
    //         if (this.ironsource) {
    //             window.NUC.event.send('chooseDragon', 'Beedragon')
    //         }

    //         this.click3 = true;
    //         this.menu.opacity = 0;
    //         this.listDragon[4].runAction(cc.sequence(
    //             cc.moveBy(1, cc.v2(0, 410)),
    //             cc.callFunc(() => {
    //                 this.listDragon[4].getComponent("MoveDragonDE4").enabled = true;
    //             })
    //         ))
    //         this.scheduleOnce(() => {
    //             this.listDragon[5].runAction(cc.sequence(
    //                 cc.moveBy(1, cc.v2(0, 410)),
    //                 cc.callFunc(() => {
    //                     this.listDragon[5].getComponent("MoveDragonDE4").enabled = true;
    //                 })
    //             ))
    //         })
    //         this.scheduleOnce(() => {
    //             this.tutorialMerge[2].zIndex = 5;
    //             this.tutorialMerge[2].active = true;
    //         }, 1)
    //     }
    // }
    clickDragonEnd() {
        this.handEnd.zIndex = 20;
        this.endDragon.active = true;
        this.listDragon[4].getComponent("MergeEnd").enabled = true;
        this.listDragon[5].getComponent("MergeEnd").enabled = true
        this.handEnd.runAction(cc.moveTo(0.01, cc.v2(this.listDragon[4].x + 45, this.listDragon[4].y - 65)))
        this.schedule(() => {
            this.handEnd.runAction(cc.sequence(cc.moveTo(1, cc.v2(this.listDragon[5].x + 15, this.listDragon[5].y - 65)),
                cc.moveTo(1, cc.v2(this.listDragon[4].x + 45, this.listDragon[4].y - 65))))
        }, 2, cc.macro.REPEAT_FOREVER, 0.01)


        // this.listDragon[6].runAction(cc.sequence(
        //     cc.moveBy(0.2, cc.v2(0, 770)),
        //     cc.callFunc(() => {
        //         this.listDragon[6].getComponent("MergeEnd").enabled = true;
        //         this.handEnd.runAction(cc.moveTo(0.01, cc.v2(this.listDragon[6].x + 40, this.listDragon[6].y - 65)))
        //         this.schedule(() => {
        //             this.handEnd.runAction(cc.sequence(cc.moveTo(1, cc.v2(this.listDragon[7].x + 10, this.listDragon[7].y - 65)),
        //                 cc.moveTo(1, cc.v2(this.listDragon[6].x + 40, this.listDragon[6].y - 65))))
        //         }, 2, cc.macro.REPEAT_FOREVER, 0.01)
        //     })
        // ))

        // this.scheduleOnce(() => {
        //     this.listDragon[7].runAction(cc.sequence(
        //         cc.moveBy(0.2, cc.v2(0, 770)),
        //         cc.callFunc(() => {
        //             this.listDragon[7].getComponent("MergeEnd").enabled = true
        //         })
        //     ))
        // })
    }


}
