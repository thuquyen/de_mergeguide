
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];
    check1: boolean = false
    check2: boolean = false
    start() {
        this.schedule(() => {
            if (this.bullet[0].isValid) {
                if (this.bullet[0].angle < 45 && !this.check1) {
                    this.bullet[0].angle += 5;
                    if (this.bullet[0].angle >= 45) {
                        this.bulletShip(this.bullet[0], 20);
                    }

                }
                else {
                    this.bullet[0].angle -= 5;
                    this.check1 = true;
                    if (this.bullet[0].angle <= -45) {
                        this.check1 = false;
                        this.bulletShip(this.bullet[0], 20);
                    }
                }
            }
        }, 0.02, cc.macro.REPEAT_FOREVER, 0.1)


        this.schedule(() => {
            if (this.bullet[1].isValid) {
                if (this.bullet[1].angle < 30 && !this.check2) {
                    this.bullet[1].angle += 5;
                    if (this.bullet[1].angle >= 30) {
                        this.bulletShip(this.bullet[1], 20);
                    }

                }
                else {
                    this.bullet[1].angle -= 5;
                    this.check2 = true;
                    if (this.bullet[1].angle <= -30) {
                        this.check2 = false;
                        this.bulletShip(this.bullet[1], 20);
                    }
                }
            }
        }, 0.02, cc.macro.REPEAT_FOREVER, 0.1)

        this.schedule(() => {
            if (this.bullet[2].isValid) {
                if (this.bullet[2].angle < 30 && !this.check2) {
                    this.bullet[2].angle += 5;
                    if (this.bullet[2].angle >= 30) {
                        this.bulletShip(this.bullet[2], 20);
                    }

                }
                else {
                    this.bullet[2].angle -= 5;
                    this.check2 = true;
                    if (this.bullet[2].angle <= -30) {
                        this.check2 = false;
                        this.bulletShip(this.bullet[2], 20);
                    }
                }
            }
        }, 0.02, cc.macro.REPEAT_FOREVER, 0.1)
    }

    private bulletShip(fire: cc.Node, timeFire: number) {
        if (fire.activeInHierarchy)
            fire.runAction(cc.sequence(cc.moveBy(timeFire, cc.v2(-2000 * Math.tan(fire.angle * Math.PI / 180), 2000)), cc.callFunc(() => {
                fire.destroy();
            })));
    }
    // update (dt) {}
}
