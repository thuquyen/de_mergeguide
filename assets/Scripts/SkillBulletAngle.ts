
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    @property()
    public timeFire: number = 0;

    @property()
    ratioScaleBullet: number = 0;

    onLoad() {
        for (let i = 0; i < this.bullet.length; i++) {
            this.bulletShip(this.bullet[i]);
        }
    }

    bulletShip(fire: cc.Node) {
        fire.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeFire, cc.v2(-2000 * Math.tan(fire.angle * Math.PI / 180), 2000)), cc.scaleBy(0.2, this.ratioScaleBullet)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    // update (dt) {}
}
