const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    e: cc.Prefab = null;
    @property([cc.Vec2])
    public points: cc.Vec2[] = [];
    @property()
    delayTime: number = 0;
    @property()
    pointX: number = 0;
    @property()
    pointY: number = 0;
    @property(cc.Node)
    pointTarget: cc.Node = null;
    @property()
    interval: number = 0;
    @property()
    delay: number = 0;

    @property([])
    arrayE: cc.Node[] = [];

    start() {
        this.scheduleOnce(() => {
            if (this.points.length == 0) {
                for (var i = 0; i < this.node.children.length; i++) {
                    this.points.push(this.node.children[i].getPosition());
                    this.spawnE(this.node.children[i].getPosition().x, this.node.children[i].getPosition().y);
                }
            }
        }, this.delayTime)

    }
    spawnE(posX: number, posY: number) {
        var e = cc.instantiate(this.e);
        e.opacity = 0;
        e.parent = this.node.parent;
        e.x = this.pointX;
        e.y = this.pointY;

        e.runAction(cc.sequence(
            cc.moveTo(1.2, this.pointX, this.pointTarget.y), cc.moveTo(0.9, posX, posY)));
        this.scheduleOnce(() => {
            e.opacity = 255;
        }, 1)
        this.arrayE.push(e);
    }

    changePos() {
        this.schedule(() => {
            for (var i = 0; i < this.node.children.length - 1; i++) {
                this.arrayE[i].runAction(
                    cc.moveTo(1, this.node.children[i + 1].getPosition().x, this.node.children[i + 1].getPosition().y))
            }
            this.arrayE[this.node.children.length - 1].runAction(
                cc.moveTo(1, this.node.children[0].getPosition().x, this.node.children[0].getPosition().y),
            );

            for (var i = 0; i < this.node.children.length - 1; i++) { //0-8
                this.node.children[i].x = this.node.children[i + 1].getPosition().x;
                this.node.children[i].y = this.node.children[i + 1].getPosition().y;
            }
            this.node.children[this.node.children.length - 1].x = this.node.children[0].getPosition().x;
            this.node.children[this.node.children.length - 1].y = this.node.children[0].getPosition().y;

        }, this.interval, cc.macro.REPEAT_FOREVER, this.delay)
    }

    // update (dt) {}
}
