
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    drawing: cc.Graphics = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    @property(cc.Node)
    list: cc.Node[] = []
    start() {
        console.log(this.list[0].y +"parent "+ this.list[0].parent.convertToNodeSpaceAR(this.list[0].position).y);

        this.drawing = this.node.getChildByName('drawing').getComponent(cc.Graphics);
        this.drawing.lineWidth = 6;
        this.drawing.moveTo(this.list[0].x, this.list[0].y);
        // this.drawing.moveTo(0, 0);
        this.drawing.lineTo(this.list[1].x, this.list[1].y);
        this.drawing.strokeColor = cc.Color.RED;
        this.drawing.stroke();
    }

    // update (dt) {}
}
