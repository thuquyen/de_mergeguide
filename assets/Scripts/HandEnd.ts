
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    dragon: cc.Node[] = []

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.runAction(cc.moveTo(0.01, cc.v2(this.dragon[0].x, this.dragon[0].y - 65)))
    }

    start() {
        this.node.zIndex = 20
        this.schedule(() => {
            this.node.runAction(cc.sequence(cc.moveTo(1, cc.v2(this.dragon[0].x, this.dragon[0].y - 65)),
                cc.moveTo(1, cc.v2(this.dragon[1].x, this.dragon[1].y - 65))))
        }, 2)
    }

    // update (dt) {}
}
