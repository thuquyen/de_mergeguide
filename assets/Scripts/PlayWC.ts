import G from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MoveDragonDE3 extends cc.Component {

    xMove: number = 0;

    yMove: number = 0;

    updateMove: boolean = false;

    statusMerge: boolean = false;

    stateButton: boolean = true;
    @property(cc.Node)
    word: cc.Node = null;
    checkWord: boolean = false;
    oneTimes: boolean = false;
    onLoad() {
        this.statusMerge = true;
        this.updateMove = true;
    }

    start() {
        
        this.xMove = this.node.x;
        this.yMove = this.node.y;

        this.node.on('touchmove', (event) => {
            console.log("move"+ this.node.name)
            let delta = event.touch.getDelta();
            this.xMove += delta.x;
            this.yMove += delta.y;
            this.updateMove = true;


            if (this.node.name.toString() !== this.word.getComponent(cc.Label).string && !this.oneTimes) {
                this.word.getComponent(cc.Label).string += this.node.name;
            }
        }, this);

        this.node.on('touchstart', (event) => {
            console.log("start"+ this.node.name)
            this.statusMerge = true;
            this.word.getComponent(cc.Label).string += this.node.name;
        })

        this.node.on('touchend', () => {
            console.log("end"+ this.node.name)
        }, this);

        this.node.on('touchcancel', () => {
            console.log("cancel"+ this.node.name)

        }, this);
    }

    update(dt) {
        if (this.checkWord) {
            this.checkWord = false;
            if (this.word.getComponent(cc.Label).string === "NOW") {
                console.log("correct Now")
            }
            this.scheduleOnce(() => {
                this.word.getComponent(cc.Label).string = "";
            }, 0.5)
        }
    }
}
