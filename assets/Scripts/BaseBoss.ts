import Global from './Global';

const { ccclass, property } = cc._decorator;

@ccclass
export default class Enemy extends cc.Component {

    @property()
    healthy: number = 0;

    @property(cc.Prefab)
    explosion: cc.Prefab = null;

    startHealthy: number = 0;

    startScale: number = 0;

    start() {
        this.startHealthy = this.healthy;
        this.startScale = this.node.scaleX;
    }


    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group === 'bulletPlayer') {
            if(Global.clickD){
                this.healthy--;
            }
            // this.spawnExplosion();
            other.node.destroy();

            if (this.healthy <= 0) {
                this.spawnExplosion();
                this.node.active = false;
            }
        }
    }

    spawnExplosion() {
        let explosion = cc.instantiate(this.explosion);
        explosion.parent = cc.Canvas.instance.node;
        explosion.x = this.node.x;
        explosion.y = this.node.y;
        let time = explosion.getComponent(cc.Animation).play('animExplosion').duration / explosion.getComponent(cc.Animation).play('animExplosion').speed;
        this.scheduleOnce(() => {
            explosion.destroy();
        }, time);
    }

    onDisable() {
        Global.numberEnemy--;
        Global.totalE--;
    }

}
