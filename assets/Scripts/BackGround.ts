import Global from './Global';
const { ccclass, property } = cc._decorator;

@ccclass
export default class BackGround extends cc.Component {

    @property()
    speed: number = 0;

    @property()
    resetBackGround: number = 0;

    start() {

    }

    update(dt) {
        // if (!Global.endGame) {
            this.node.y -= this.speed * dt;
            if (this.node.y < -this.resetBackGround) {
                this.node.y = 0;
            }
        // }
    }
}
