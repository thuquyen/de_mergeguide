const { ccclass, property } = cc._decorator;

@ccclass
export default class HandChoose extends cc.Component {

    @property(cc.Node)
    card: cc.Node[] = [];
    @property()
    timeActive: number = 0;
    @property(cc.Node)
    handNode: cc.Node = null;
    @property()
    numberShip: number = 1;
    // LIFE-CYCLE CALLBACKS:
    // @property(cc.Node)
    // hand: cc.Node = null;
    // @property(cc.Node)
    // handIdel: cc.Node = null;
    onLoad() {
        this.schedule(() => {
            // this.handIdel.opacity = 255;
            // this.hand.opacity = 0;
            if (this.card[0].isValid && this.card[1].isValid) {
                this.handNode.runAction(cc.sequence(cc.moveTo(0.5, this.card[this.numberShip].getPosition().x + 10, this.card[this.numberShip].getPosition().y -8),
                    cc.callFunc(() => {
                        // this.handIdel.opacity = 0;
                        // this.hand.opacity = 255;
                        // this.scheduleOnce(() => {
                        //     this.hand.opacity = 0;
                        //     this.handIdel.opacity = 255
                        // }, 1.4);
                    })));

                this.numberShip++;
                if (this.numberShip > 1) {
                    this.numberShip = 0;
                }
            }
        }, 1.8, cc.macro.REPEAT_FOREVER, this.timeActive);
    }

    // update (dt) {}
}
