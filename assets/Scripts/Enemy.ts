import Global from './Global';

const { ccclass, property } = cc._decorator;

@ccclass
export default class Enemy extends cc.Component {

    @property()
    healthy: number = 0;

    @property(cc.Node)
    barHealthy: cc.Node = null;

    @property(cc.Prefab)
    bullets: cc.Prefab = null;

    @property()
    timeSpeed: number = 0;

    @property()
    timeIntervalBulet: number = 0;

    @property(cc.Prefab)
    explosion: cc.Prefab = null;

    timeRandomBullet: number = 500;

    timeReset: number = 0;

    updateBullet: boolean = true;

    bulletPool: cc.NodePool = null;

    startHealthy: number = 0;

    startScale: number = 0;

    start() {
        if (this.barHealthy)
            this.barHealthy.active = false;
        this.startHealthy = this.healthy;
        this.startScale = this.node.scaleX;
        this.bulletPool = new cc.NodePool();
        let bullet = null;
        for (let i = 0; i < 10; i++) {
            bullet = cc.instantiate(this.bullets);
            this.bulletPool.put(bullet);
        }
        // console.log(this.bulletPool.size());
    }

    spawnBullet() {
        Global.maxBulletEnemy++;
        let bullet = null;
        if (this.bulletPool.size() > 0) {
            bullet = this.bulletPool.get();
        } else {
            bullet = cc.instantiate(this.bullets);
        }
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x;
        bullet.y = this.node.y;

        bullet.runAction(cc.sequence(cc.moveBy(this.timeSpeed, cc.v2(0, -1000)), cc.callFunc(() => {
            this.bulletPool.put(bullet);
            Global.maxBulletEnemy--;
            if (Global.maxBulletEnemy <= 0) {
                Global.maxBulletEnemy = 0;
            }
        })));

    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group === 'bulletPlayer') {
            this.healthy--;
            // if (other.node.name === 'bullet_Volvaria_2' ) {
            //     this.healthy--;
            // }

            // if (other.node.name === 'bullet_enus') {
            //     this.healthy -= 2;
            // }

            // if (other.node.name === 'bulletCircle') {
            //     this.healthy -= 2;
            // }
            // if (other.node.name === 'Bullet_16') {
            //     this.healthy -= 3;
            // }

            // if (other.node.name === 'Bullet_enus_1') {
            //     this.healthy -= 4;
            // }

            other.node.destroy();

            if (this.barHealthy) {
                this.barHealthy.active = true;
                this.barHealthy.scaleX = this.healthy / this.startHealthy * this.startScale;
            }

            if (this.healthy <= 0) {
                this.spawnExplosion();
                this.node.active = false;
            }
        }
    }

    spawnExplosion() {
        let explosion = cc.instantiate(this.explosion);
        explosion.parent = cc.Canvas.instance.node;
        explosion.x = this.node.x;
        explosion.y = this.node.y;
        let time = explosion.getComponent(cc.Animation).play('animExplosion').duration / explosion.getComponent(cc.Animation).play('animExplosion').speed;
        this.scheduleOnce(() => {
            explosion.destroy();
        }, time);
    }

    onDisable() {
        if (this.node.name =='z7E') {
            Global.totalEIdle++;
        }
        if (this.node.name =='z5E') {
            Global.totalEFly++;
        }
        Global.numberEnemy--;
        Global.totalE--;
        // console.log("total E" + Global.totalE);
        // if (Global.numberEnemy <= 0) {
        //     if (Global.onComplete)
        //         Global.onComplete();
        // }
    }

    update(dt) {
        // if (Global.maxBulletEnemy < 3) {
        //     if (this.updateBullet) {
        //         if (Date.now() > this.timeIntervalBulet + this.timeReset + this.timeRandomBullet * Math.random()) {
        //             this.timeReset = Date.now();
        //             this.spawnBullet();
        //         }
        //     }
        // }

        if (Global.endGame) {
            this.updateBullet = false;
        }

    }

}
