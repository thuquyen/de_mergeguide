import Global from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TimeEgg extends cc.Component {

    // @property(cc.Label)
    // strNumberDragon: cc.Label = null;

    @property(cc.Node)
    barTimeEgg: cc.Node = null;

    @property()
    timeEgg: number = 0;

    // @property(cc.Node)
    // handTouch: cc.Node = null;

    startScale: number = 0;

    startBarTimeEgg: number = 0;

    stateAnim: boolean = true;

    start() {
        // this.handTouch.zIndex = 5;
        // this.handTouch.setPosition(this.node.getPosition());
        this.startScale = this.barTimeEgg.scaleX;
        this.startBarTimeEgg = this.timeEgg;
    }

    update(dt) {
        if (Global.clickEgg) {
            this.timeEgg -= dt * 30;
            if (this.timeEgg <= 0) {
                this.timeEgg = 0;
                Global.clickEgg = false;
            }
        }
         else {
            this.timeEgg += dt * 7;
            if (this.timeEgg >= this.startBarTimeEgg) {
                this.timeEgg = this.startBarTimeEgg;

                // if (Global.numberDragon > 4) {
                //     Global.numberDragon = 5;
                //     Global.clickEgg = false;
                //     this.timeEgg = this.startBarTimeEgg;
                // } else {
                //     this.timeEgg = 0;
                //     Global.clickEgg = true;
                //     // Global.numberDragon++;
                // }
            }
        }

        if (Global.start) {
            if (Global.numberDragon == 0) {
                this.stateAnim = true;
                this.node.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');
            } else {
                if (this.stateAnim) {
                    this.stateAnim = false;
                    this.node.getChildByName('EGG').getComponent(cc.Animation).play('animEggLoop');
                }
            }
        }
        // this.strNumberDragon.string = Global.numberDragon.toString();
        this.barTimeEgg.scaleX = this.timeEgg / this.startBarTimeEgg * this.startScale;
    }

}
