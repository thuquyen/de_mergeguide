import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE5 extends cc.Component {
    @property(cc.Node)
    iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    btnEgg: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    @property(cc.Node)
    btnIns: cc.Node = null;

    @property(cc.Node)
    handTouch: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node[] = [];

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 2;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;
    adcolony: boolean = false;

    run1st: boolean = true;
    run2nd: boolean = true;
    run3rd: boolean = true;

    totalWave: number = 0;
    checkTotalW: boolean = false;
    checkWin1: boolean = false;
    checkeWin2: boolean = false;

    @property(cc.Node)
    menu: cc.Node = null;
    click1: boolean = false;
    click2: boolean = false;
    click3: boolean = false;
    click4: boolean = false;
    checkauto: boolean = false;
    isClickEgg: boolean = false;

    @property(cc.Node)
    wave: cc.Node[] = [];

    @property(cc.Node)
    waveNext: cc.Node[] = [];
    nextWave1: boolean = null;
    nextWave2: boolean = null;

    isEnd: boolean = false;
    @property(cc.Node)
    bgEnd: cc.Node = null;

    @property(cc.Node)
    hatchEggMenu: cc.Node[] = [];

    @property(cc.Node)
    continueBtn: cc.Node = null;

    onLoad() {
        this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        this.bgEnd.zIndex = 9;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;

        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
            }
        }, this);
        // this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');

        if (this.adcolony) {
            window.mraid.isViewable;
        }
    }

    start() {
        this.scheduleOnce(() => {
            this.hatchEgg();
            if (this.ironsource)
                window.NUC.trigger.autoplay();
        }, 7);
        this.endGame.zIndex = 10;
        this.btnEgg.zIndex = 2;
        //click to open egg
        this.btnEgg.on('click', this.spawnDragonClick, this);
    }

    hatchEgg() {
        this.wave[0].active = true;
        this.wave[1].active = true;

        this.tutorialMerge[0].destroy();
        for (var i = 0; i < 2; i++) {
            this.hatchEggMenu[i].destroy();
        }
        this.hatchEggMenu[3].runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, this.btnEgg.x, this.btnEgg.y), cc.scaleTo(0.7, 1, 1)), cc.callFunc(() => {
            this.hatchEggMenu[3].destroy();
            this.btnEgg.opacity = 255;
            this.btnEgg.getComponent(cc.Button).enabled = true;
            this.btnIns.active = true;
            this.spawnDIntital();
        })))

        this.hatchEggMenu[2].runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, this.btnEgg.x, this.btnEgg.y), cc.scaleTo(0.7, 0, 0)), cc.callFunc(() => {
            this.hatchEggMenu[2].destroy();
        })))
    }

    spawnDIntital() {
        this.spawnDragon(this.dragons[0]);
        this.spawnDragon(this.dragons[1]);
        this.scheduleOnce(() => {
            this.spawnDragon(this.dragons[2]);
        }, 0.15)
        this.scheduleOnce(() => {
            this.spawnDragon(this.dragons[0]);
            this.tutorialMerge[1].zIndex = 5;
            this.tutorialMerge[1].active = true;
        }, 0.2)
    }

    showHandClick() {
        this.handTouch.active = true;
        this.handTouch.zIndex = 10;
        this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');
    }

    update(dt) {
        if (Global.totalE == 0 && !this.checkTotalW) {
            this.checkTotalW = true;
            Global.endGame = true;
            if (this.ironsource && !this.checkWin1) {
                this.checkWin1 = true;
                window.NUC.trigger.endGame('win');
            }
            this.scheduleOnce(() => {
                this.showEndGame();
            }, 1.5)
        }

        if (Global.totalEIdle >= 11 && !this.nextWave1 && !Global.endGame) {
            this.nextWave1 = true;
            this.waveNext[0].getComponent("WaveEChangePos").enabled = true;
        }
        if (Global.totalEFly >= 9 && !this.nextWave2 && !Global.endGame) {
            this.nextWave2 = true;
            this.waveNext[1].getComponent("path").enabled = true;
        }

        if (Global.mergeNumber == 1 && this.run1st) {
            this.showHandClick();
            this.run1st = false;
            this.isClickEgg = true;
            this.tutorialMerge[1].destroy();
            this.tutorialMerge[2].zIndex = 5;
            this.tutorialMerge[2].active = true;
            this.spawnDragon(this.dragons[3]);
        }
        else if (Global.mergeNumber == 2 && this.run2nd) {
            this.run2nd = false;
            this.spawnDragon(this.dragons[0]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[2]);
            }, 0.5)
        }
        else if (Global.mergeNumber == 3 && this.run3rd) {
            this.run3rd = false;
            this.spawnDragon(this.dragons[1]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[1]);
            }, 0.5)
        }
        else if (Global.mergeNumber >= 9) {
            if (this.ironsource && !this.checkeWin2) {
                this.checkeWin2 = true;
                window.NUC.trigger.endGame('win')
            }
            if (!this.isEnd) {
                Global.endGame = true;
                this.isEnd = true;
                var findDeep = function (node, nodeName) {
                    if (node.name == nodeName) return node;
                    for (var i = 0; i < node.children.length; i++) {
                        var res = findDeep(node.children[i], nodeName);
                        if (res)
                            res.destroy();
                        // return res;
                    }
                }
                findDeep(cc.director.getScene(), "z2E");
                findDeep(cc.director.getScene(), "z5E");
                findDeep(cc.director.getScene(), "z6E");
                findDeep(cc.director.getScene(), "z7E");

                this.scheduleOnce(() => {
                    this.showEndGame();
                }, 1.5)
            }
        }
    }

    spawnDragon(dragonNode: cc.Prefab) {
        if (this.stateStart) {
            this.stateStart = false;
        }
        Global.start = true;
        // when clicked guide text will off
        // if (this.handTouch.isValid) {
        //     this.handTouch.opacity = 0;
        //     this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
        // }
        // if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
        cc.Camera.main.getComponent(cc.Animation).play("animCamera");
        Global.numberDragonInScreen++;
        Global.numberDragon--;
        if (Global.numberDragon <= 0) {
            Global.numberDragon = 0;
        }
        Global.clickEgg = true;
        let dragon = cc.instantiate(dragonNode);
        this.spawnfxEgg();
        dragon.parent = cc.Canvas.instance.node;
        dragon.x = this.btnEgg.x;
        dragon.y = this.btnEgg.y;
        dragon.scale = 0.2;
        dragon.getComponent("MoveDragonDE4").enabled = false;
        this.spawnfxFlyDragon(dragon);
        dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.47), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
            cc.scaleTo(0.8, 0.8).easing(cc.easeSineOut())),
            cc.callFunc(() => {
                dragon.getComponent("MoveDragonDE4").enabled = true;

            })));
        // }
    }

    spawnDragonClick() {
        // when clicked guide text will off
        if (this.handTouch.isValid) {
            this.handTouch.opacity = 0;
            this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
        }
        if (this.isClickEgg) {
            this.tutorialMerge[2].destroy();
            this.isClickEgg = false;
            // when clicked guide text will off
            if (this.handTouch.isValid) {
                this.handTouch.opacity = 0;
                this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
            }

            // if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            cc.Camera.main.getComponent(cc.Animation).play("animCamera");
            Global.numberDragonInScreen++;
            Global.numberDragon--;
            if (Global.numberDragon <= 0) {
                Global.numberDragon = 0;
            }
            let dragon = cc.instantiate(this.dragons[0]);
            this.spawnfxEgg();
            dragon.parent = cc.Canvas.instance.node;
            dragon.x = this.btnEgg.x;
            dragon.y = this.btnEgg.y;
            dragon.scale = 0.2;
            dragon.getComponent("MoveDragonDE4").enabled = false;
            this.spawnfxFlyDragon(dragon);
            dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.5), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                cc.callFunc(() => {
                    dragon.getComponent("MoveDragonDE4").enabled = true;
                })));

            let dragon1 = cc.instantiate(this.dragons[1]);
            this.spawnfxEgg();
            dragon1.parent = cc.Canvas.instance.node;
            dragon1.x = this.btnEgg.x;
            dragon1.y = this.btnEgg.y;
            dragon1.scale = 0.2;
            dragon1.getComponent("MoveDragonDE4").enabled = false;
            this.spawnfxFlyDragon(dragon1);
            dragon1.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.5), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                cc.callFunc(() => {
                    dragon1.getComponent("MoveDragonDE4").enabled = true;
                })));
            // }
        }
    }

    spawnfxEgg() {
        let fxEgg = cc.instantiate(this.fxClickEgg);
        fxEgg.parent = cc.Canvas.instance.node;
        fxEgg.x = this.btnEgg.x - 5;
        fxEgg.y = this.btnEgg.y + 10;
        fxEgg.zIndex = 4;
    }

    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        this.bgEnd.active = true;
        this.logo.active = false;
        if (this.stateShowEndGame) {
            var findDeep = function (node, nodeName) {
                if (node.name == nodeName) return node;
                for (var i = 0; i < node.children.length; i++) {
                    var res = findDeep(node.children[i], nodeName);
                    if (res)
                        res.destroy();
                    // return res;
                }
            }
            findDeep(cc.director.getScene(), "z2E");
            findDeep(cc.director.getScene(), "z5E");
            findDeep(cc.director.getScene(), "z6E");
            findDeep(cc.director.getScene(), "z7E");

            this.btnIns.active = false;
            this.stateShowEndGame = false;
            this.endGame.active = true;
            this.scheduleOnce(() => {
                this.continueBtn.getComponent(cc.Animation).play();
            }, 0.5)
            // cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            this.btnEgg.destroy();

            Global.endGame = true;
        }

    }
}