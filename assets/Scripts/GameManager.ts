import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE6 extends cc.Component {

    stateStart: boolean = true;
    onLoad() {

        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
    }
    start() {
        this.draw();
    }
    draw() {
        
        this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
            // remember location so we know where to draw the first line from
            this.lastPos = event.getLocation();
        }, this.node)

        this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            var g = this.getComponent(cc.Graphics);
            // set start of line to the end of the previous line
            g.moveTo(this.lastPos.x, this.lastPos.y);
            // draw line to location of touch
            g.lineTo(event.getLocation().x, event.getLocation().y);
            g.stroke();

            // remember location of touch for start of next line
            this.lastPos = event.getLocation();
        }, this.node);
    }

}
