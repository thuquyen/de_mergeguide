
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    strAnimCurrent: string = '';
    // LIFE-CYCLE CALLBACKS:

    @property(cc.Prefab)
    bullet: cc.Prefab[] = [];

    private skillBoss: cc.Prefab = null;

    @property()
    iBullet: number = 0;

    @property()
    interval: number = 0;

    @property()
    delay: number = 0;

    onLoad() {
        this.changeAnimation(0, 'FORM2_attack4', true);
        this.getComponent(sp.Skeleton).setMix('FORM2_attack4', 'FORM2_idle', 0.1);
        this.getComponent(sp.Skeleton).setMix('FORM2_idle', 'FORM2_attack5', 0.1);
        this.getComponent(sp.Skeleton).setMix('FORM2_attack5', 'FORM2_attack2', 0.1);
        this.getComponent(sp.Skeleton).setMix('FORM2_attack2', 'FORM2_attack5', 0.1);
        this.getComponent(sp.Skeleton).setMix('FORM2_attack5', 'FORM2_idle', 0.1);
    }

    start() {
        //MOVE
        this.node.runAction(cc.sequence(cc.spawn(cc.moveTo(3.5, cc.v2(0, 230)), cc.scaleTo(1, 1)), cc.callFunc(() => {
            this.changeAnimation(0, 'FORM2_idle', true);
            this.scheduleOnce(() => {
            this.schedule(() => {
                let x = (Math.random() * 2 - 1) * 50;
                // console.log("postion Boss " + x)
                // this.node.runAction(cc.sequence(cc.moveTo(2, cc.v2(x, this.node.y)), cc.callFunc(() => {
                // if (G.hpBoss)
                this.bulletBoss1(120, 105);
                // })));

            }, this.interval, cc.macro.REPEAT_FOREVER, this.delay);
            }, 1);

        })))
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }

    skill() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'FORM2_attack2', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'FORM2_attack5', true);
                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'FORM2_idle', true);
                }, 4.7);
            }, 7.5);
        } else if (this.iBullet === 0) {
            this.changeAnimation(1, 'FORM2_attack5', true);
        }
    }

    skillScaleBullet(scaleNum: number, xTemp: number, yTemp: number) {
        this.skillBoss = this.bullet[this.iBullet];
        var newBullet = cc.instantiate(this.skillBoss);
        newBullet.parent = cc.Canvas.instance.node;
        // newBullet.x = (Math.round(Math.random()) - 1) * Math.random() * 200;;
        // newBullet.y = -190 - Math.random() * 60;
        newBullet.x = xTemp;
        newBullet.y = yTemp;
        newBullet.zIndex = 10
        newBullet.runAction(cc.sequence((cc.scaleTo(0.22, scaleNum, scaleNum)),
            cc.callFunc(() => {
                if (newBullet.activeInHierarchy) {
                    this.scheduleOnce(() => {
                        newBullet.destroy();
                    }, 0.7)
                }
            })))
    }
    skillSpiderSilk() {
        for (var i = 0; i < 6; i++) {
            var xTemp = this.node.width * ((Math.random() * 2 - 1) * 0.3);
            // var yTemp = -this.node.height / 2 * (Math.random() * 0.8);
            var yTemp = -200 - Math.random() * 50
            var scaleNum = Math.random()*2;
            if (scaleNum > 1) {
                this.skillScaleBullet(scaleNum, xTemp, yTemp)
            }
        }
    }
    public bulletBoss1(xTemp: number, yTemp: number) {
        this.skill();
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
        }

        if (this.iBullet == 0) {
            this.scheduleOnce(() => {
                cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                this.skillSpiderSilk();

                this.scheduleOnce(() => {
                    cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                    this.skillSpiderSilk();
                }, 3.8)
            }, 1.9)

        }
        else {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y;

                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.angle = 30;
                    newBullet.x = this.node.x;
                    newBullet.y = this.node.y;

                    this.scheduleOnce(() => {
                        this.skillBoss = this.bullet[this.iBullet];
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.angle = 15;
                        newBullet.x = this.node.x;
                        newBullet.y = this.node.y;
                        this.scheduleOnce(() => {
                            this.skillBoss = this.bullet[this.iBullet];
                            var newBullet = cc.instantiate(this.skillBoss);
                            newBullet.parent = cc.Canvas.instance.node;
                            newBullet.x = this.node.x;
                            newBullet.y = this.node.y;

                            this.scheduleOnce(() => {
                                this.skillBoss = this.bullet[this.iBullet];
                                var newBullet = cc.instantiate(this.skillBoss);
                                newBullet.parent = cc.Canvas.instance.node;
                                newBullet.angle = 30;
                                newBullet.x = this.node.x;
                                newBullet.y = this.node.y;
                
                            }, 0.75)
                        }, 0.5)
                    }, 0.7)
                }, 0.65)
            }, 0.3)
        }
    }
    // update (dt) {}
}
