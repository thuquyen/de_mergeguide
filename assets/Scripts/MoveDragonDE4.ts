import Global from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MoveDragonDE3 extends cc.Component {

    @property(cc.Prefab)
    dragon: cc.Prefab = null

    @property(cc.Prefab)
    fxMerge: cc.Prefab = null;

    @property(cc.Prefab)
    prefabBullets: cc.Prefab = null;

    @property()
    timeSpeedBullet: number = 0;

    @property()
    timeBullet: number = 200;

    @property()
    healthyPlayer: number = 0;

    updateBullets: boolean = true;

    timeResetBullet: number = 0;

    public bullets: cc.NodePool = null;

    xMove: number = 0;

    yMove: number = 0;

    updateMove: boolean = false;

    statusMerge: boolean = false;

    arrBullet: cc.Node[] = [];

    stateEndGame: boolean = true;

    stateButton: boolean = true;

    @property(cc.Prefab)
    explosion: cc.Prefab = null;

    start() {
        this.node.zIndex = 3;
        this.bullets = new cc.NodePool();
        for (let i = 0; i < 10; i++) {
            let bullet = cc.instantiate(this.prefabBullets);
            this.bullets.put(bullet);
        }

        this.xMove = this.node.x;
        this.yMove = this.node.y;

        this.node.on('touchmove', (event) => {
            Global.moveD = true;
            this.statusMerge = true;
            let delta = event.touch.getDelta();
            this.xMove += delta.x;
            this.yMove += delta.y;
            this.updateMove = true;
        }, this);

        this.node.on('touchstart', () => {
            Global.moveD = true;
        })

        this.node.on('touchend', () => {
            Global.moveD = true;
            this.statusMerge = false;
        }, this);
        this.node.on('touchcancel', () => {
            Global.moveD = true;
            this.statusMerge = false;
        }, this);
    }

    update(dt) {

        if (this.updateMove) {
            this.node.x = cc.misc.lerp(this.node.x, this.xMove, 0.4);
            this.node.y = cc.misc.lerp(this.node.y, this.yMove, 0.4);
            this.node.x = cc.misc.clampf(this.node.x, -this.node.parent.width / 2, this.node.parent.width / 2);
            this.node.y = cc.misc.clampf(this.node.y, -this.node.parent.height / 2 + 95, this.node.parent.height / 2);
        }

        if (this.updateBullets) {
            if (Date.now() > this.timeResetBullet + this.timeBullet) {
                this.timeResetBullet = Date.now();
                if (this.node.name == "thanosTop" || this.node.name == "box_dragon4_2" || this.node.name == "Firedragon_top" || this.node.name == "box_dragon6Top" || this.node.name == 'box_23_Nostradamus' || this.node.name == 'box_volvaria' || this.node.name == 'box_6_Tarrel') {
                    this.creatBulletCircle();
                }
                else if (this.node.name == "BubbleWyvern_top") {
                    this.creatBullet2(280);
                } else if (this.node.name == "BlueEye_top" || this.node.name == 'box_24_Lycan') {
                    this.creatBullet2(50);
                }
                else if (this.node.name == "box_dragon8_de4") {
                    this.creatBullet2(0);
                }
                else if (this.node.name == "RedEye_top") {
                    this.creatBullet2(-25);
                }
                else if (this.node.name == 'box_26_Necrophos' || this.node.name == 'box_4_Bahamut') {
                    this.creatBulletRandom(20);
                }
                else
                    this.creatBullet(40);
            }
        }

        if (Global.endGame) {
            if (this.stateEndGame) {
                this.stateEndGame = false;
                this.updateBullets = false;
                this.updateMove = false;
                this.node.off('touchmove');
                this.node.off('touchstart');
                this.node.off('touchend');
                this.scheduleOnce(() => {
                    this.node.runAction(cc.moveBy(1, cc.v2(0, 1000)).easing(cc.easeBackIn()));
                }, 1 * Math.random());
            }
        }
    }

    onCollisionStay(other, self) {
        if (this.statusMerge) {
            if (other.node.name === self.node.name) {
                if (this.dragon) {
                    this.updateBullets = false;
                    this.updateMove = false;
                    other.node.destroy();
                    self.node.destroy();
                    Global.numberDragonInScreen--;
                    // cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                    if (cc.Canvas.instance.node.getChildByName("bgLayout")) {
                        if (cc.Canvas.instance.node.getChildByName("bgLayout").isValid)
                            cc.Canvas.instance.node.getChildByName("bgLayout").destroy();
                    }
                    if (Global.mergeNumber == 1) {
                        if (cc.Canvas.instance.node.getChildByName("tutorialMerge2")) {
                            if (cc.Canvas.instance.node.getChildByName("tutorialMerge2").isValid)
                                cc.Canvas.instance.node.getChildByName("tutorialMerge2").destroy();
                        }
                    }
                    if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                        if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                            cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
                    }

                    if (this.stateButton) {
                        this.stateButton = false;
                        if (cc.Canvas.instance.node.getChildByName("button")) {
                            cc.Canvas.instance.node.getChildByName("button").runAction(cc.moveTo(2, cc.v2(0, -410)).easing(cc.easeBackOut()));
                        }
                    }
                    this.spawn(other, self, this.dragon);
                    this.spawn(other, self, this.fxMerge);
                    Global.mergeNumber++;
                } else {
                    return;
                }
            }
        }
    }

    onCollisionEnter(other, self) {
        if (other.node.group === 'bulletEnemy') {
            this.healthyPlayer--;
            if (this.healthyPlayer < 0) {
                this.spawnExplosion();
                this.node.opacity = 140;
                this.updateBullets = false;
                return;
            }
            other.node.destroy();
        }

    }
    spawnExplosion() {
        let explosion = cc.instantiate(this.explosion);
        explosion.parent = cc.Canvas.instance.node;
        explosion.x = this.node.x;
        explosion.y = this.node.y;
        let time = explosion.getComponent(cc.Animation).play('animExplosion').duration / explosion.getComponent(cc.Animation).play('animExplosion').speed;
        this.scheduleOnce(() => {
            explosion.destroy();
        }, time);
    }

    spawn(other, self, prefab) {
        let dragon = cc.instantiate(prefab);
        dragon.parent = cc.Canvas.instance.node;
        dragon.runAction(
            cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())
        )
        dragon.x = (other.node.x + self.node.x) / 2;
        dragon.y = (other.node.y + self.node.y) / 2;
        dragon.zIndex = 4;
    }

    onDestroy() {
        for (let i = 0; i < this.arrBullet.length; i++) {
            this.bullets.put(this.arrBullet[i]);
        }
    }

    creatBullet(tempY: number) {
        let bullet = null;
        if (this.bullets.size() > 0) {
            bullet = this.bullets.get();
        } else {
            bullet = cc.instantiate(this.prefabBullets);
        }
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x;
        bullet.y = this.node.y + tempY;
        // bullet.scale = 0;
        bullet.zIndex = 2;
        this.arrBullet.push(bullet);
        bullet.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeSpeedBullet, cc.v2(0, 1000)), cc.scaleTo(bullet.scaleX, bullet.scaleY)), cc.callFunc(() => {
            if (this.bullets)
                this.bullets.put(bullet);
        })));
    }

    creatBulletCircle() {
        let bullet = null;
        bullet = cc.instantiate(this.prefabBullets);
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x;
        bullet.y = this.node.y + 30;
    }
    creatBullet2(temp: number) {
        let bullet = null;
        bullet = cc.instantiate(this.prefabBullets);
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x;
        bullet.y = this.node.y + temp;
        bullet.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeSpeedBullet, cc.v2(0, 1000)), cc.scaleTo(bullet.scaleX, bullet.scaleY)), cc.callFunc(() => {
            if (bullet)
                bullet.destroy();
        }))
        )
    }
    creatBulletRandom(temp: number) {
        let bullet = null;
        bullet = cc.instantiate(this.prefabBullets);
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x - 10;
        bullet.y = this.node.y + temp;
        bullet.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeSpeedBullet, cc.v2(0, 1000)), cc.scaleTo(bullet.scaleX, bullet.scaleY)), cc.callFunc(() => {
            if (bullet)
                bullet.destroy();
        }))
        )
        this.scheduleOnce(() => {
            let bullet = null;
            bullet = cc.instantiate(this.prefabBullets);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.node.x + 10;
            bullet.y = this.node.y + temp - 5;
            bullet.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeSpeedBullet, cc.v2(0, 1000)), cc.scaleTo(bullet.scaleX, bullet.scaleY)), cc.callFunc(() => {
                if (bullet)
                    bullet.destroy();
            }))
            )
        }, 0.35)
        this.scheduleOnce(() => {
            let bullet = null;
            bullet = cc.instantiate(this.prefabBullets);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.node.x;
            bullet.y = this.node.y + temp - 5;
            bullet.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeSpeedBullet, cc.v2(0, 1000)), cc.scaleTo(bullet.scaleX, bullet.scaleY)), cc.callFunc(() => {
                if (bullet)
                    bullet.destroy();
            }))
            )
        }, 0.7)
    }
}
