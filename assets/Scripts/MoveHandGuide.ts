
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // @property(cc.Node)
    listDragon: cc.Node[] = [];

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    update(dt) {
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    this.listDragon[0].x = res.x;
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "box_dragon4_2");
        for (var i = 0; i < this.listDragon.length; i++) {
            console.log("pos"+ this.listDragon[i].x);
        }
    }

    // update (dt) {}
}
