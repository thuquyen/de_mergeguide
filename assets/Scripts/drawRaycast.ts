
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    touches: cc.Vec2[] = []
    graphics: cc.Graphics = null;

    posX1: number = 0;
    posY1: number = 0;
    posX2: number = 0;
    poxY2: number = 0;

    start() {
        var canvas = cc.find('Canvas');
        canvas.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        canvas.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        canvas.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);

        this.graphics = this.getComponent(cc.Graphics);
    }

    onTouchStart(event) {
        this.touches.length = 0;
        this.touches.push(event.touch.getLocation());
        this.posX1 = event.touch.getLocation().x;
        this.posY1 = event.touch.getLocation().y;
    }

    onTouchMove(event) {
        let touches = this.touches;
        touches.push(event.touch.getLocation());
        this.posX2 = event.touch.getLocation().x;
        this.poxY2 = event.touch.getLocation().y;
        const MIN_POINT_DISTANCE = 20;
        
        //clear khi so sánh chữ xong
        this.graphics.clear();

        let worldPos = this.node.convertToWorldSpaceAR(cc.v2());
        this.graphics.moveTo(touches[0].x - worldPos.x, touches[0].y - worldPos.y);
        let lastIndex = 0;
        // for (let i = 1, l = touches.length; i < l; i++) {
        //     if (touches[i].sub(touches[lastIndex]).mag() < MIN_POINT_DISTANCE) {
        //         continue;
        //     }
        //     lastIndex = i;
        //     this.graphics.lineTo(touches[i].x - worldPos.x, touches[i].y - worldPos.y);
        // }
        // this.graphics.stroke();
        this.graphics.lineWidth = 6;  
        this.graphics.strokeColor = cc.Color.RED;
        this.graphics.moveTo(this.posX1, this.posY1);
        this.graphics.lineTo(this.posX2, this.poxY2);
        this.graphics.stroke();
    }

    onTouchEnd(event) {
    }
}