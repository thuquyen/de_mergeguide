
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    // @property()
    // public timeFire: number = 0;
    start() {
        this.node.scale = 0.3
        this.node.runAction(cc.scaleTo(1, 1.2, 1.2).easing(cc.easeSineOut()));
        this.node.runAction(cc.sequence(
            cc.moveTo(1, cc.v2(this.node.x, 180 + Math.random() * 30)),
            cc.callFunc(() => {
                this.node.getComponent(cc.Animation).enabled = false;
                for (let i = 0; i < this.bullet.length; i++) {
                    if (i <= 2 && this.bullet[i].activeInHierarchy) {
                        this.bulletShip(this.bullet[i], 2000, 5);
                    }
                    else if (i > 2 && i <= 4 && this.bullet[i].activeInHierarchy) {
                        this.bulletShip(this.bullet[i], 0, 190);
                    }
                    else {
                        this.bulletShip(this.bullet[i], -2000, 5);
                    }

                }
            })
        ))
    }

    private bulletShip(fire: cc.Node, axisY: number, timeFire: number) {
        if (fire.activeInHierarchy)
            fire.runAction(cc.sequence(cc.moveBy(timeFire, cc.v2(-2000 * Math.tan(fire.angle * Math.PI / 180), axisY)), cc.callFunc(() => {
                fire.destroy();
            })));
    }

}
