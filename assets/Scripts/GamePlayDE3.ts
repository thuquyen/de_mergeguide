import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE3 extends cc.Component {

    // @property(cc.Node)
    // iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    // @property(cc.Node)
    // newDragon: cc.Node = null;

    @property(path)
    path1: path = null;

    // @property(path)
    // path2: path = null;

    @property(cc.Node)
    btnEgg: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    // @property(cc.Node)
    // notification: cc.Node = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    @property(cc.Node)
    button: cc.Node = null;

    // @property(cc.Node)
    // handTouch: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node = null;
    @property(cc.Node)
    tutorialMerge2: cc.Node = null;

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 10;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;

    @property(cc.Node)
    listDragon: cc.Node[] = [];
    run1st: boolean = true;
    run2nd: boolean = true;
    run3rd: boolean = true;
    onLoad() {
        //when dragon fly complete, player can click move it 
        this.listDragon[0].runAction(cc.sequence(
            cc.moveBy(1.1, cc.v2(0, 465)),
            cc.callFunc(() => {
                this.listDragon[0].getComponent("MoveDragonDE3").enabled = true;
            })
        ))
        this.listDragon[1].runAction(cc.sequence(
            cc.moveBy(1.1, cc.v2(0, 465)),
            cc.callFunc(() => {
                this.listDragon[1].getComponent("MoveDragonDE3").enabled = true;
            })
        ))
        this.scheduleOnce(() => {
            this.tutorialMerge.zIndex = 5;
            this.tutorialMerge.active = true;
        }, 1.3)

        // this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        //auto play
        // this.scheduleOnce(() => {
        //     if (this.run1st) {
        //         this.run1st = false;
        //         this.spawnDragon(this.dragons[0]);
        //         this.scheduleOnce(() => {
        //             this.tutorialMerge2.zIndex = 5;
        //             this.tutorialMerge2.active = true;
        //         }, 0.8)
        //         this.scheduleOnce(() => {
        //             if (this.run2nd) {
        //                 this.run2nd = false;
        //                 this.spawnDragon(this.dragons[1]);
        //                 this.scheduleOnce(() => {
        //                     this.spawnDragon(this.dragons[1]);
        //                 }, 0.5)
        //                 this.scheduleOnce(() => {
        //                     if (this.run3rd) {
        //                         this.run3rd = false;
        //                         this.scheduleOnce(() => {
        //                             this.showEndGame();
        //                         }, 0.3)
        //                     }
        //                 }, 4)
        //             }
        //         }, 4)
        //     }
        // }, 4)
        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
                // this.scheduleOnce(() => {
                //     if (this.ironsource)
                //         window.NUC.trigger.autoplay();
                //     this.showEndGame();
                // }, 300);
            }
        }, this);
        // this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');
    }

    start() {
        // this.button.zIndex = 6;
        this.endGame.zIndex = 10;
        // this.notification.zIndex = 4;
        this.btnEgg.zIndex = 2;
        //click to open egg
        // this.btnEgg.on('click', this.spawnDragon, this);

        this.path1.onComlete = () => {
            this.path1.destroy();
            this.showEndGame();
            // this.path2.node.active = true;
        }

        // this.path2.onComlete = () => {
        //     this.path2.destroy();
        //     this.showEndGame();
        // }
    }
    update(dt) {
        if (Global.mergeNumber == 1 && this.run1st) {
            this.run1st = false;
            this.spawnDragon(this.dragons[0]);
            this.scheduleOnce(() => {
                this.tutorialMerge2.zIndex = 5;
                this.tutorialMerge2.active = true;
            }, 0.8)
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[1]);
            }, 0.5)
        }
        else if (Global.mergeNumber == 2 && this.run2nd) {
            this.run2nd = false;
            this.spawnDragon(this.dragons[2]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[0]);
            }, 0.5)
        }
        else if (Global.mergeNumber == 3 && this.run3rd) {
            this.run3rd = false;
            this.scheduleOnce(() => {
                this.showEndGame();
            }, 1.5)
        }
    }

    spawnDragon(dragonNode: cc.Prefab) {
        if (this.stateStart) {
            this.stateStart = false;
        }

        Global.start = true;
        //when clicked guide text will off
        // if (this.handTouch.isValid)
        //     this.handTouch.destroy();

        if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            // if (Global.numberDragon <= 0) {
            //     for (let i = 0; i < this.notification.childrenCount; i++) {
            //         if (this.notification.children[i].name === "txt_eggIsNotReady" || this.notification.children[i].name === "frame_shop_2") {
            //             this.notification.children[i].active = true;
            //         } else {
            //             this.notification.children[i].active = false;
            //         }
            //     }
            //     this.notification.getComponent(cc.Animation).play("animNoti");

            //     if (this.stateNoti) {
            //         this.stateNoti = false;
            //         this.scheduleOnce(() => {
            //             this.notification.getComponent(cc.Animation).play("animNotiOff");
            //             this.stateNoti = true;
            //         }, 2);
            //     }

            // } else
            {
                cc.Camera.main.getComponent(cc.Animation).play("animCamera");
                Global.numberDragonInScreen++;
                Global.numberDragon--;
                if (Global.numberDragon <= 0) {
                    Global.numberDragon = 0;
                }
                Global.clickEgg = true;
                let dragon = cc.instantiate(dragonNode);
                this.spawnfxEgg();
                dragon.parent = cc.Canvas.instance.node;
                dragon.x = this.btnEgg.x;
                dragon.y = this.btnEgg.y;
                dragon.scale = 0.2;
                dragon.getComponent("MoveDragonDE3").enabled = false;
                this.spawnfxFlyDragon(dragon);
                dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.4), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                    cc.scaleTo(1, 1).easing(cc.easeSineOut())),
                    cc.callFunc(() => {
                        dragon.getComponent("MoveDragonDE3").enabled = true;
                    })));
            }
        }
        //  else {
        //     for (let i = 0; i < this.notification.childrenCount; i++) {
        //         if (this.notification.children[i].name === "txt_maxDragon" || this.notification.children[i].name === "frame_shop_2") {
        //             this.notification.children[i].active = true;
        //         } else {
        //             this.notification.children[i].active = false;
        //         }
        //     }
        //     this.notification.getComponent(cc.Animation).play("animNoti");

        //     if (this.stateNoti) {
        //         this.stateNoti = false;
        //         this.scheduleOnce(() => {
        //             this.notification.getComponent(cc.Animation).play("animNotiOff");
        //             this.stateNoti = true;
        //         }, 2);
        //     }
        // }
    }

    spawnfxEgg() {
        let fxEgg = cc.instantiate(this.fxClickEgg);
        fxEgg.parent = cc.Canvas.instance.node;
        fxEgg.x = this.btnEgg.x - 5;
        fxEgg.y = this.btnEgg.y + 10;
        fxEgg.zIndex = 4;
    }

    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        if (this.stateShowEndGame) {
            if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
            }
            if (cc.Canvas.instance.node.getChildByName("tutorialMerge2")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge2").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge2").destroy();
            }
            if (this.ironsource)
                window.NUC.trigger.endGame('fake');
            if (cc.Canvas.instance.node.getChildByName("bgLayout")) {
                if (cc.Canvas.instance.node.getChildByName("bgLayout").isValid)
                    cc.Canvas.instance.node.getChildByName("bgLayout").destroy();
            }

            if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
            }
            // this.scheduleOnce(() => {
            //     this.newDragon.runAction(cc.moveTo(2, cc.v2(0, -330)));
            // }, 0.4);
            this.stateShowEndGame = false;
            Global.endGame = true;
            this.endGame.active = true;
            cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            this.btnEgg.off('click');
            if (this.button)
                this.button.active = false;
        }
    }

}
