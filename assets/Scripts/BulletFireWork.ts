const { ccclass, property } = cc._decorator;

@ccclass
export default class skillDelay extends cc.Component {
    @property(cc.Node)
    private fire: cc.Node = null;
    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;
    @property()
    public timeFire: number = 0;
    onLoad() {
        this.getFire();
    }
    getFire() {
        this.fire.runAction(cc.sequence(cc.moveBy(this.timeFire-3, 0, 1000), cc.callFunc(() => {
            this.fire.destroy();
        })));
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.bulletPrefab);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.node.x + 15;
            bullet.y = this.node.y - 10;
            bullet.runAction(cc.sequence(cc.moveBy(this.timeFire, 0, 2000), cc.callFunc(() => {
                bullet.destroy();
            })));
        }, 0.1)
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.bulletPrefab);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.node.x - 10;
            bullet.y = this.node.y - 20;
            bullet.runAction(cc.sequence(cc.moveBy(this.timeFire, 0, 2000), cc.callFunc(() => {
                bullet.destroy();
            })));
        }, 0.2)
    }
}
