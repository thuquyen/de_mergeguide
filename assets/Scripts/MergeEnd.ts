import Global from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MoveDragonDE3 extends cc.Component {

    xMove: number = 0;

    yMove: number = 0;

    updateMove: boolean = false;
    statusMerge: boolean = false;

    @property(String)
    androidLink: string = ''
    @property(String)
    iosLink: string = ''
    @property(String)
    defaultLink: string = ''
    posX: number = 0;
    posY: number = 0;
    start() {
        this.node.zIndex = 3;

        this.posX = this.node.x;
        this.posY = this.node.y;

        this.xMove = this.node.x;
        this.yMove = this.node.y;

        this.node.on('touchmove', (event) => {
            this.statusMerge = true;
            let delta = event.touch.getDelta();
            this.xMove += delta.x;
            this.yMove += delta.y;
            this.updateMove = true;
        }, this);
        this.node.on('touchstart', () => {
            this.updateMove = true;
             this.node.runAction(cc.moveTo(0.1, cc.v2(this.posX, this.posY)))
        }, this)
        this.node.on('touchend', () => {
            this.updateMove = false;
            this.node.runAction(cc.moveTo(0.1, cc.v2(this.posX, this.posY)))
            this.xMove = this.posX
            this.yMove = this.posY;
            this.statusMerge = false;
        }, this);
        this.node.on('touchcancel', () => {
            this.updateMove = false;
            this.node.runAction(cc.moveTo(0.1, cc.v2(this.posX, this.posY)))
            this.xMove = this.posX;
            this.yMove = this.posY;
            this.statusMerge = false;
        }, this);
    }

    update(dt) {

        if (this.updateMove) {
            this.node.x = cc.misc.lerp(this.node.x, this.xMove, 0.3);
            this.node.y = cc.misc.lerp(this.node.y, this.yMove, 0.3);
            // this.node.x = cc.misc.clampf(this.node.x, -this.node.parent.width / 2, this.node.parent.width / 2);
            // this.node.y = cc.misc.clampf(this.node.y, -this.node.parent.height / 2 + 95, this.node.parent.height / 2);
        }
    }

    onCollisionStay(other, self) {
        if (this.statusMerge ) {
            if (other.node.name === self.node.name) {
                {
                    this.openAdUrl();
                }
            }
        }
    }

    openAdUrl() {
        var clickTag = '';
        window.androidLink = this.androidLink;
        window.iosLink = this.iosLink;
        window.defaultLink = this.defaultLink;
        if (window.openAdUrl) {
            window.openAdUrl();
        } else {
            window.open();
        }
    }
}
