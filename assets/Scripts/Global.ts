import player from "./PlayWC"
export default {
    numberEnemy: 0,
    clickEgg: false,
    numberDragon: 2,
    numberDragonInScreen: 0,
    maxBulletEnemy: 0,
    endGame: false,
    start: false,
    mergeNumber: 0,
    totalE: 1,
    isTransform: false,
    isTransform2: false,
    posX: 0,
    posY:0,
    touchPos: null,
    totalEIdle: 0,
    totalEFly: 0,
    clickD:false,
    moveD: false
}

interface Global {
    touchPos: cc.Vec2,

    hitBullet: cc.AudioClip,
    soundBg: cc.AudioSource,
    endSound: cc.AudioClip,
    chooseNode: boolean,
    checkColision1:boolean,
    checkColision2: boolean,
    totalBlock: number,
    clickStart: boolean
}

let Global: Global = {
    touchPos: null,
    hitBullet: null,
    soundBg: null,
    endSound: null,
    chooseNode: false,
    checkColision1: false,
    checkColision2:false,
    totalBlock: 0,
    clickStart: false
}

