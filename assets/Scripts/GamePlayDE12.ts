import Global from "./Global";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE5 extends cc.Component {
    @property(cc.Node)
    iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    // @property(cc.Node)
    // btnIns: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node[] = [];
    showGuide: boolean = false;

    @property(cc.Node)
    notification: cc.Node = null;

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 5;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;
    adcolony: boolean = false;

    run1st: boolean = true;
    run2nd: boolean = true;
    run3rd: boolean = true;

    totalWave: number = 0;
    checkTotalW: boolean = false;
    checkWin1: boolean = false;
    checkeWin2: boolean = false;

    @property(cc.Node)
    menuDragon: cc.Node[] = [];

    click: number = 0;
    checkauto: boolean = false;
    isClickEgg: boolean = false;

    isEnd: boolean = false;

    @property(cc.Node)
    continueBtn: cc.Node = null;

    @property(cc.Node)
    arrowChoose: cc.Node[] = [];

    @property(cc.Node)
    menuChoose: cc.Node = null;

    @property(cc.Node)
    handMoveGuide: cc.Node = null;
    posHandX1: number = 0;
    posHandY1: number = 0;
    posHandX2: number = 0;
    posHandY2: number = 0;

    check1: boolean = false;
    check2: boolean = false;
    check3: boolean = false;

    @property(cc.Layout)
    bgE: cc.Layout = null;
    onLoad() {
        this.handMoveGuide.zIndex = 10;
        this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        this.bgE.node.zIndex = 9;
        this.endGame.zIndex = 10;
        this.notification.zIndex = 10;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;

        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
            }
        }, this);

        if (this.adcolony) {
            window.mraid.isViewable;
        }
    }
    openTurn1() {
        this.scheduleOnce(() => {
            this.menuDragon[1].opacity = 255;
            this.menuDragon[3].opacity = 255;
        }, 1)
    }
    openTurn2() {
        this.scheduleOnce(() => {
            this.menuDragon[2].opacity = 255;
            this.menuDragon[4].opacity = 255;
        }, 1)
    }

    playArrow(arrow: cc.Node) {
        arrow.opacity = 255;
        arrow.getComponent(cc.Animation).play();
    }
    pauseArrow(arrow: cc.Node) {
        arrow.opacity = 0;
        arrow.getComponent(cc.Animation).stop();
    }

    start() {
        this.scheduleOnce(() => {
            this.spawnDragon(this.dragons[0]);
            this.menuDragon[0].opacity = 255;
            this.playArrow(this.arrowChoose[0]);
        }, 4)

        // this.menuDragon[1].opacity = 255;

        // this.scheduleOnce(() => {
        //     if (this.ironsource)
        //         window.NUC.trigger.autoplay();
        // }, 7);
        // this.endGame.zIndex = 10;
    }
    spawnD1() {
        if (this.menuDragon[0].opacity == 255) {
            for (var i = 0; i < 4; i++) {
                this.pauseArrow(this.arrowChoose[i]);
            }
            this.spawnDragon(this.dragons[0]);
            this.menuDragon[0].opacity = 130;
            this.scheduleOnce(() => {
                this.menuDragon[0].opacity = 255
            }, 5)
        }
    }
    spawnD2() {
        if (this.menuDragon[1].opacity == 255) {
            for (var i = 0; i < 4; i++) {
                this.pauseArrow(this.arrowChoose[i]);
            }
            this.spawnDragon(this.dragons[1]);
            this.menuDragon[1].opacity = 130;
            this.scheduleOnce(() => {
                this.menuDragon[1].opacity = 255
            }, 6)
        }
    }
    spawnD3() {
        if (this.menuDragon[2].opacity == 255) {
            for (var i = 0; i < 4; i++) {
                this.pauseArrow(this.arrowChoose[i]);
            }
            this.spawnDragon(this.dragons[2]);
            this.menuDragon[2].opacity = 130;
            this.scheduleOnce(() => {
                this.menuDragon[2].opacity = 255
            }, 7)
        }
    }
    spawnD4() {
        if (this.menuDragon[3].opacity == 255) {
            for (var i = 0; i < 4; i++) {
                this.pauseArrow(this.arrowChoose[i]);
            }
            this.spawnDragon(this.dragons[3]);
            this.menuDragon[3].opacity = 130;
            this.scheduleOnce(() => {
                this.menuDragon[3].opacity = 255
            }, 8)

            if (!this.check3 && !this.run1st) {
                this.check3 = true;
                this.menuDragon[1].opacity = 255;
                this.playArrow(this.arrowChoose[1]);
            }
        }
    }
    spawnD5() {
        if (this.menuDragon[4].opacity == 255) {
            this.spawnDragon(this.dragons[4]);
            this.menuDragon[4].opacity = 130;
            this.scheduleOnce(() => {
                this.menuDragon[4].opacity = 255
            }, 10)
        }
    }

    update(dt) {
        if (Global.totalE == 0 && !this.checkTotalW) {
            this.checkTotalW = true;
            Global.endGame = true;
            if (this.ironsource && !this.checkWin1) {
                this.checkWin1 = true;
                window.NUC.trigger.endGame('win');
            }
            this.scheduleOnce(() => {
                this.showEndGame();
            }, 1.5)
        }

        if (Global.mergeNumber == 1 && this.run1st) {
            this.run1st = false;
            this.isClickEgg = true;
            this.tutorialMerge[1].destroy();
            this.playArrow(this.arrowChoose[3])
        }
    }

    spawnDragon(dragonNode: cc.Prefab) {
        if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            Global.start = true;
            this.click++;
            if (this.click == 1) {
                this.tutorialMerge[0].zIndex = 5;
                this.tutorialMerge[0].active = true;
            }
            if (this.click == 2) {
                Global.clickD = true;
                if (!this.showGuide) {
                    this.showGuide = true;
                    this.tutorialMerge[0].destroy();
                    this.tutorialMerge[1].zIndex = 5;
                    this.tutorialMerge[1].active = true;
                }
                this.openTurn1();
            }
            if (this.click == 3) {
                this.openTurn2();
            }

            cc.Camera.main.getComponent(cc.Animation).play("animCamera");
            Global.numberDragonInScreen++;
            Global.numberDragon--;
            if (Global.numberDragon <= 0) {
                Global.numberDragon = 0;
            }
            let dragon = cc.instantiate(dragonNode);
            dragon.parent = cc.Canvas.instance.node;
            dragon.x = Math.random() * 200;
            dragon.y = -450;
            dragon.scale = 0.2;
            dragon.getComponent("MoveDragonDE4").enabled = false;
            this.spawnfxFlyDragon(dragon);
            dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.4), -this.node.height / 2 * (Math.random() * 0.5))).easing(cc.easeSineOut()),
                cc.scaleTo(0.8, 0.8).easing(cc.easeSineOut())),
                cc.callFunc(() => {
                    dragon.getComponent("MoveDragonDE4").enabled = true;
                    // this.scheduleOnce(()=>{
                    if (!this.check1 && this.click == 1) {
                        this.check1 = true;
                        this.posHandX1 = dragon.x + 15;
                        this.posHandY1 = dragon.y - 45;
                        this.handMoveGuide.x = this.posHandX1;
                        this.handMoveGuide.y = this.posHandY1;
                    }
                    if (!this.check2 && this.click == 2) {
                        this.check2 = true;
                        this.posHandX2 = dragon.x + 15;
                        this.posHandY2 = dragon.y - 45;
                        this.handMoveGuide.opacity = 255;
                        this.schedule(() => {
                            if (Global.moveD) {
                                this.handMoveGuide.opacity = 0;
                            }
                            this.handMoveGuide.runAction(cc.sequence(
                                cc.moveTo(0.7, cc.v2(this.posHandX2, this.posHandY2)),
                                cc.moveTo(0.7, cc.v2(this.posHandX1, this.posHandY1))
                            ))
                        }, 1.4, cc.macro.REPEAT_FOREVER, 0.01)
                    }
                    // },1)

                })));
        }
        else {
            this.notification.children[1].active = true;
            this.notification.getComponent(cc.Animation).play("animNoti");
            this.scheduleOnce(() => {
                this.notification.getComponent(cc.Animation).play("animNotiOff");
            }, 1.5);
        }
    }
    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        this.bgE.node.opacity = 100;
        this.menuChoose.destroy();
        this.logo.destroy();
        if (this.stateShowEndGame) {
            var findDeep = function (node, nodeName) {
                if (node.name == nodeName) return node;
                for (var i = 0; i < node.children.length; i++) {
                    var res = findDeep(node.children[i], nodeName);
                    if (res)
                        res.destroy();
                    // return res;
                }
            }
            findDeep(cc.director.getScene(), "z2E");
            findDeep(cc.director.getScene(), "z5E");
            findDeep(cc.director.getScene(), "z6E");
            findDeep(cc.director.getScene(), "z7E");

            // this.btnIns.active = false;
            this.stateShowEndGame = false;
            this.endGame.active = true;
            this.scheduleOnce(() => {
                this.continueBtn.getComponent(cc.Animation).play();
            }, 0.5)
            // cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            Global.endGame = true;
        }

    }
}