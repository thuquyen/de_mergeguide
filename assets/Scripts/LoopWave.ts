const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    e: cc.Prefab = null;
    @property([cc.Vec2])
    public points: cc.Vec2[] = [];
    @property()
    delayTime: number = 0;
    @property()
    pointX: number = 0;
    @property()
    pointY: number = 0;
    @property()
    spawnX: number = 0;
    @property()
    spawnY: number = 0;
    @property(cc.Node)
    pointTarget: cc.Node = null;
    @property()
    interval: number = 0;
    @property()
    delay: number = 0;
    @property([])
    arrayE: cc.Node[] = [];
    totalSpawn: number = 0;
    @property()
    timeSpawn: number = 0;
    check: boolean = false;
    spawn1e() {
        // this.schedule(() => {
        if (this.totalSpawn <= 1) {
            var e = cc.instantiate(this.e);
            e.opacity = 0;
            e.parent = this.node.parent;
            e.x = this.pointX;
            e.y = this.pointY;
            var i = 0;
            this.schedule(() => {
                if (!this.check) {
                    e.runAction(cc.sequence(
                        cc.moveTo(0.5, this.node.children[i].getPosition().x, this.node.children[i].getPosition().y),
                        cc.callFunc(() => {
                            //br21
                            if (i == 1) {
                                e.opacity = 255;
                            }
                            i++;
                            if (i == this.node.children.length) {
                                this.check = true;
                                // e.opacity = 0;
                                // i = 0;
                            }
                        })
                    ))
                }
            }, 0.38)
            this.totalSpawn++;
        }
        //     }, this.timeSpawn)
    }

    onLoad() {
        //spawn and move 1 enemy
        this.spawn1e();
        // this.movePath();

    }
}
