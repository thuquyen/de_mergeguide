
const {ccclass, property} = cc._decorator;

@ccclass
export default class Egg extends cc.Component {

    @property()
    speed: number = 0;

    @property()
    resetBackGround: number = 0;

    start () {

    }

    update (dt) {
        this.node.x += this.speed * dt;
        if(this.node.x > this.resetBackGround){
            this.node.x = 0;
        }
    }
}
