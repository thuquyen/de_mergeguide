// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class NewClass extends cc.Component {

    @property()
    androidLink: string = '';

    @property()
    iosLink: string = '';

    @property()
    defaultLink: string = '';

    start() {
        this.node.on('touchmove', this.openAdUrl, this);
        this.node.on('touchstart', this.openAdUrl, this);
    }

    openAdUrl() {
        var clickTag = '';
        window.androidLink = this.androidLink;
        window.iosLink = this.iosLink;
        window.defaultLink = this.defaultLink;
        if (window.openAdUrl) {
            window.openAdUrl();
        } else {
            window.open();
        }
    }
}
