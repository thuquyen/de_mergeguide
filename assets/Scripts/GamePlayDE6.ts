import Global from "./Global";
import path from "./path";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class GamePlayDE6 extends cc.Component {

    @property(cc.Node)
    iconGooglePlay: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    btnEgg: cc.Node = null;

    @property(cc.Prefab)
    fxClickEgg: cc.Prefab = null;

    @property(cc.Node)
    cloud: cc.Node = null;

    @property(cc.Prefab)
    fxFlyDragon: cc.Prefab = null;

    // @property(cc.Node)
    // notification: cc.Node = null;

    @property(cc.Node)
    endGame: cc.Node = null;

    @property(cc.Node)
    btnIns: cc.Node = null;

    @property(cc.Node)
    handTouch: cc.Node = null;

    @property(cc.Node)
    tutorialMerge: cc.Node = null;

    @property(cc.Node)
    tutorialMerge1: cc.Node = null;

    @property(cc.Node)
    tutorialMerge2: cc.Node = null;

    @property(cc.Prefab)
    dragons: cc.Prefab[] = [];

    maxDragonInScreen: number = 10;

    stateNoti: boolean = true;

    stateStart: boolean = true;

    stateShowEndGame: boolean = true;

    ironsource: boolean = false;

    @property(cc.Node)
    listDragon: cc.Node[] = [];
    run1st: boolean = true;
    run2nd: boolean = true;
    run3rd: boolean = true;

    @property(cc.Node)
    wave2: cc.Node = null;
    @property(cc.Node)
    wave3: cc.Node = null;

    totalWave: number = 0;
    checkTotalW: boolean = false;
    checkWin1: boolean = false;
    checkeWin2: boolean = false;

    @property(cc.Node)
    handMoveGuide: cc.Node = null;

    @property(cc.Node)
    handMoveGuide2: cc.Node = null;

    click1: boolean = false;
    click2: boolean = false;
    click3: boolean = false;
    click4: boolean = false;
    checkauto: boolean = false;
    isMerge1: boolean = false;

    isclick: boolean = false;
    onLoad() {
        this.iconGooglePlay.zIndex = 100;
        this.logo.zIndex = 10;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        this.node.on('touchstart', () => {
            if (this.stateStart) {
                if (this.ironsource)
                    window.NUC.trigger.interaction();
                this.stateStart = false;
                this.scheduleOnce(() => {
                    this.tutorialMerge.active = false;
                    if (this.ironsource)
                        window.NUC.trigger.autoplay();
                    this.showEndGame();
                }, 27);
            }
        }, this);

        this.handTouch.zIndex = 10;
        this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");
        this.btnEgg.getChildByName('EGG').getComponent(cc.Animation).play('animEggIdle');

        this.listDragon[0].runAction(cc.sequence(
            cc.moveBy(1.1, cc.v2(0, 400)),
            cc.callFunc(() => {
                this.listDragon[0].getComponent("MoveDragonDE4").enabled = true;
                this.tutorialMerge.active = true;
                this.tutorialMerge.zIndex = 10;
                this.handTouch.active = true;
                this.handMoveGuide.runAction(cc.moveTo(0.1, cc.v2(this.listDragon[0].x, this.listDragon[0].y)));
            })
        ))

        this.scheduleOnce(() => {
            this.wave2.getComponent("path").enabled = true;
        }, 5)
        this.scheduleOnce(() => {
            this.wave3.getComponent("path").enabled = true;
        }, 5)
    }

    start() {
        // this.button.zIndex = 6;
        this.endGame.zIndex = 10;
        // this.notification.zIndex = 4;
        this.cloud.zIndex = 1;
        this.btnEgg.zIndex = 2;
        //click to open egg
        this.btnEgg.on('click', this.spawnD, this);
        this.scheduleOnce(() => {
            if (!this.isclick) {
                this.isclick = true;
                this.spawnD();
                this.scheduleOnce(() => {
                    this.tutorialMerge.active = false;
                    if (this.ironsource)
                        window.NUC.trigger.autoplay();
                    this.showEndGame();
                }, 25);
            }
        }, 5.5)
    }
    update(dt) {
        if (Global.totalE == 0 && !this.checkTotalW) {
            this.checkTotalW = true;
            if (this.ironsource && !this.checkWin1) {
                this.checkWin1 = true;
                window.NUC.trigger.endGame('win');
                window.NUC.event.send('DetailWin', 'allEDie')
            }
            this.showEndGame();
        }
        if (Global.mergeNumber == 1 && this.run1st) {
            this.isMerge1 = false;
            this.handTouch.opacity = 255;
            this.handTouch.getComponent(cc.Animation).play("animHandTouchOn");

            this.tutorialMerge1.active = false;
            //off hand guide after merge 1st
            this.handMoveGuide.destroy();
            this.run1st = false;
            this.spawnDragon(this.dragons[0]);
        }
        else if (Global.mergeNumber == 2 && this.run2nd) {
            this.handMoveGuide2.destroy();
            this.run2nd = false;
            this.spawnDragon(this.dragons[2]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[0]);
            }, 0.5)
        }
        else if (Global.mergeNumber == 3 && this.run3rd) {
            this.run3rd = false;
            this.spawnDragon(this.dragons[0]);
            this.scheduleOnce(() => {
                this.spawnDragon(this.dragons[2]);
            }, 0.5)
        }
        else if (Global.mergeNumber >= 4) {
            if (this.ironsource && !this.checkeWin2) {
                this.checkeWin2 = true;
                window.NUC.trigger.endGame('win')
                window.NUC.event.send('DetailWin', 'mergerComplete')
            }
            this.scheduleOnce(() => {
                this.showEndGame();
            }, 2.5)
        }
    }

    spawnD() {
        this.isclick = true;
        if (!this.isMerge1) {
            this.isMerge1 = true;
            this.tutorialMerge.active = false;
            if (Global.numberDragon == 1) {
                this.handTouch.destroy();
            }
            Global.start = true;
            if (this.handTouch.isValid) {
                this.handTouch.opacity = 0;
                this.handTouch.getComponent(cc.Animation).play("animHandTouchOff");
            }
            cc.Camera.main.getComponent(cc.Animation).play("animCamera");
            Global.numberDragonInScreen++;

            if (Global.numberDragon == 2) {
                this.tutorialMerge1.zIndex = 5;
                this.tutorialMerge1.active = true;

                Global.numberDragon--;
                Global.clickEgg = true;
                let dragon = cc.instantiate(this.dragons[0]);
                this.spawnfxEgg();
                dragon.parent = cc.Canvas.instance.node;
                dragon.x = this.btnEgg.x;
                dragon.y = this.btnEgg.y;
                dragon.scale = 0.2;
                dragon.getComponent("MoveDragonDE4").enabled = false;
                this.spawnfxFlyDragon(dragon);
                dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() - 0.5) * 0.8), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                    cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                    cc.callFunc(() => {
                        dragon.getComponent("MoveDragonDE4").enabled = true;
                        this.handMoveGuide.zIndex = 10;
                        this.handMoveGuide.runAction(cc.sequence(cc.moveTo(0.01, cc.v2(dragon.x + 10, dragon.y - 10)),
                            cc.callFunc(() => {
                                this.handMoveGuide.opacity = 255;
                                //if user don't play then after 3s, game auto play
                                // this.scheduleOnce(() => {
                                //     this.listDragon[0].runAction(cc.moveTo(1.2, cc.v2(dragon.x, dragon.y)))
                                // }, 3)
                            })
                        ));
                        this.schedule(() => {
                            if (Global.numberDragon >= 1 && this.listDragon[0].isValid && dragon.isValid) {
                                this.handMoveGuide.runAction(cc.sequence(
                                    cc.moveTo(1, cc.v2(this.listDragon[0].x + 10, this.listDragon[0].y - 10)),
                                    cc.moveTo(1, cc.v2(dragon.x + 10, dragon.y - 10)),
                                ))
                            }
                        }, 2, cc.macro.REPEAT_FOREVER, 0.1)
                    })));
            }
            else {
                if (Global.numberDragon == 1) {
                    this.scheduleOnce(() => {
                        this.tutorialMerge2.zIndex = 5;
                        this.tutorialMerge2.active = true;
                    }, 0.7)

                    Global.numberDragon -= 2;
                    Global.clickEgg = true;
                    let dragon = cc.instantiate(this.dragons[1]);
                    this.spawnfxEgg();
                    dragon.parent = cc.Canvas.instance.node;
                    dragon.x = this.btnEgg.x;
                    dragon.y = this.btnEgg.y;
                    dragon.scale = 0.2;
                    dragon.getComponent("MoveDragonDE4").enabled = false;
                    this.spawnfxFlyDragon(dragon);
                    dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() - 0.5) * 0.8), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                        cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                        cc.callFunc(() => {
                            dragon.getComponent("MoveDragonDE4").enabled = true
                        })));

                    let dragonOther = cc.instantiate(this.dragons[1]);
                    this.spawnfxEgg();
                    dragonOther.parent = cc.Canvas.instance.node;
                    dragonOther.x = this.btnEgg.x;
                    dragonOther.y = this.btnEgg.y;
                    dragonOther.scale = 0.2;
                    dragonOther.getComponent("MoveDragonDE4").enabled = false;
                    this.spawnfxFlyDragon(dragonOther);
                    dragonOther.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() - 0.5) * 0.8), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                        cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                        cc.callFunc(() => {
                            dragonOther.getComponent("MoveDragonDE4").enabled = true;
                            this.handMoveGuide2.zIndex = 10;
                            this.handMoveGuide2.runAction(cc.sequence(cc.moveTo(0.01, cc.v2(dragon.x + 10, dragon.y - 10)),
                                cc.callFunc(() => {
                                    this.handMoveGuide2.opacity = 255;
                                })
                            ));
                            this.schedule(() => {
                                if (dragonOther.isValid && dragon.isValid) {
                                    this.handMoveGuide2.runAction(cc.sequence(
                                        cc.moveTo(1, cc.v2(dragonOther.x + 10, dragonOther.y - 10)),
                                        cc.moveTo(1, cc.v2(dragon.x + 10, dragon.y - 10)),
                                    ))
                                }
                            }, 2, cc.macro.REPEAT_FOREVER, 0.1)
                        })));
                }
            }
        }
    }

    spawnDragon(dragonNode: cc.Prefab) {
        if (Global.numberDragonInScreen <= this.maxDragonInScreen) {
            cc.Camera.main.getComponent(cc.Animation).play("animCamera");
            Global.numberDragonInScreen++;
            Global.clickEgg = true;
            let dragon = cc.instantiate(dragonNode);
            this.spawnfxEgg();
            dragon.parent = cc.Canvas.instance.node;
            dragon.x = this.btnEgg.x;
            dragon.y = this.btnEgg.y;
            dragon.scale = 0.2;
            dragon.getComponent("MoveDragonDE4").enabled = false;
            this.spawnfxFlyDragon(dragon);
            dragon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.7, cc.v2(this.node.width * ((Math.random() * 2 - 1) * 0.4), -this.node.height / 2 * (Math.random() * 0.5) - 50)).easing(cc.easeSineOut()),
                cc.scaleTo(0.85, 0.85).easing(cc.easeSineOut())),
                cc.callFunc(() => {
                    dragon.getComponent("MoveDragonDE4").enabled = true;
                })));
        }
    }

    spawnfxEgg() {
        let fxEgg = cc.instantiate(this.fxClickEgg);
        fxEgg.parent = cc.Canvas.instance.node;
        fxEgg.x = this.btnEgg.x - 5;
        fxEgg.y = this.btnEgg.y + 10;
        fxEgg.zIndex = 4;
    }

    spawnfxFlyDragon(parent) {
        let fxFlyDragon = cc.instantiate(this.fxFlyDragon);
        fxFlyDragon.parent = parent;
        fxFlyDragon.x = 0;
        fxFlyDragon.y = 0;
        fxFlyDragon.zIndex = 4;
    }

    showEndGame() {
        if (this.stateShowEndGame) {
            if (this.handTouch.isValid) {
                this.handTouch.destroy();
            }
            if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
            }
            if (cc.Canvas.instance.node.getChildByName("tutorialMerge1")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge1").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge1").destroy();
            }
            if (cc.Canvas.instance.node.getChildByName("tutorialMerge2")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge2").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge2").destroy();
            }

            if (cc.Canvas.instance.node.getChildByName("bgLayout")) {
                if (cc.Canvas.instance.node.getChildByName("bgLayout").isValid)
                    cc.Canvas.instance.node.getChildByName("bgLayout").destroy();
            }

            if (cc.Canvas.instance.node.getChildByName("tutorialMerge")) {
                if (cc.Canvas.instance.node.getChildByName("tutorialMerge").isValid)
                    cc.Canvas.instance.node.getChildByName("tutorialMerge").destroy();
            }
            // this.scheduleOnce(() => {
            //     this.newDragon.runAction(cc.moveTo(2, cc.v2(0, -330)));
            // }, 0.7);
            this.btnIns.active = false;
            this.stateShowEndGame = false;
            Global.endGame = true;
            this.endGame.active = true;
            cc.director.getCollisionManager().enabled = false;
            this.node.off('touchstart');
            this.node.off('touchmove');
            this.node.off('touchend');
            this.btnEgg.off('click');
            // if (this.iconGooglePlay)
            //     this.iconGooglePlay.active = false;
        }
    }

    clickIns1() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnStart')
        }
    }
    clickIns2() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnEnd')
        }
    }

}
